
Tomato 25.0 5.0 0.0 3.0 1.0 1.0
Apple 130.0 34.0 0.0 25.0 1.0 5.0
Avocado 50.0 3.0 4.5 0.0 1.0 1.0
Banana 110.0 30.0 0.0 19.0 1.0 3.0
Cantaloupe 50.0 12.0 0.0 11.0 1.0 1.0
Grapefruit 60.0 23.0 0.0 20.0 0.0 2.0
Grapes 90.0 23.0 0.0 20.0 0.0 1.0
HoneydewMelon 50.0 12.0 0.0 11.0 1.0 1.0
Kiwi 90.0 20.0 1.0 13.0 1.0 4.0
Lemon 15.0 5.0 0.0 2.0 0.0 2.0
Lime 20.0 7.0 0.0 0.0 0.0 2.0
Nectarine 60.0 15.0 0.5 11.0 1.0 2.0
Orange 80.0 19.0 0.0 14.0 1.0 3.0
Peach 60.0 15.0 0.5 13.0 1.0 2.0
Pear 100.0 26.0 0.0 16.0 1.0 6.0
Pineapple 50.0 13.0 0.0 10.0 1.0 1.0
Plums 70.0 19.0 0.0 16.0 1.0 2.0
Strawberries 50.0 11.0 0.0 8.0 1.0 2.0
SweetCherries 100.0 26.0 0.0 16.0 1.0 1.0
Tangerine 0.0 13.0 0.0 9.0 1.0 2.0
Watermelon 80.0 21.0 0.0 20.0 1.0 1.0
Asparagus 20.0 4.0 0.0 2.0 2.0 2.0
BellPepper 25.0 6.0 0.0 4.0 1.0 2.0
Broccoli 45.0 8.0 0.5 2.0 4.0 3.0
Carrot 30.0 7.0 0.0 5.0 1.0 2.0
Cauliflower 25.0 5.0 0.0 2.0 2.0 2.0
Celery 15.0 4.0 0.0 2.0 0.0 2.0
Cucumber 10.0 2.0 0.0 1.0 1.0 1.0
GreenBeans 20.0 5.0 0.0 2.0 1.0 3.0
GreenCabbage 25.0 5.0 0.0 2.0 1.0 3.0
GreenOnion 10.0 2.0 0.0 1.0 0.0 1.0
IcebergLettuce 10.0 2.0 0.0 1.0 1.0 1.0
LeafLEttuce 15.0 2.0 0.0 1.0 1.0 1.0
Mushrooms 20.0 3.0 0.0 0.0 3.0 1.0
Onion 45.0 11.0 0.0 9.0 1.0 3.0
Potato 110.0 25.0 0.0 1.0 3.0 2.0
Radishes 10.0 3.0 0.0 2.0 0.0 1.0
SummerSquash 20.0 4.0 0.0 2.0 1.0 2.0
SweetCorn 90.0 18.0 2.5 5.0 4.0 2.0
SweetPotato 100.0 23.0 0.0 7.0 2.0 4.0
Meat 250.0 0.0 15.0 0.0 26.0 0.0
BreadItalian 77.0 14.0 1.0 0.0 2.0 1.0
BreadRye 73.0 14.0 1.0 0.0 2.0 2.0
BreadPupernickel 71.0 13.0 1.0 0.0 2.0 2.0
BreadWholeWheat 78.0 15.0 1.0 6.0 3.0 2.0
Oatmeal 76.0 14.0 1.0 2.0 2.0 1.0
BreadEggbread 81.0 14.0 2.0 1.0 3.0 1.0
BreadWhiteEnrichedPita 165.0 33.0 1.0 1.0 5.0 1.0
BreadPitaWholeWheat 170.0 35.0 2.0 1.0 6.0 5.0
BreadEzikiel 80.0 15.0 1.0 5.0 4.0 3.0
BreadBanana 200.0 35.0 6.0 0.0 4.0 1.0
Croissant 231.0 26.0 12.0 6.0 5.0 2.0
RollCiabatta 180.0 35.0 1.0 0.0 7.0 1.0
RollPretzel 200.0 37.0 2.0 0.0 6.0 1.0
SushiRainbowRoll 476.0 50.0 16.0 0.0 33.0 6.0
CinnamonRoll 420.0 57.0 18.0 26.0 8.0 2.0
CheeseCheddar 536.0 2.0 45.0 0.0 32.0 0.0
CheeseFeta 396.0 6.0 32.0 6.0 21.0 0.0
CheeseGouda 101.0 5.0 8.0 1.0 7.0 0.0
CheeseSwiss 502.0 7.0 37.0 2.0 36.0 0.0
CheeseProvolone 463.0 3.0 35.0 1.0 34.0 0.0
CheeseColby 520.0 3.0 42.0 1.0 31.0 0.0
CheeseBrie 95.0 0.0 8.0 0.0 6.0 0.0
CheeseRomano 110.0 1.0 8.0 0.0 9.0 0.0
CheeseCheshire 110.0 1.0 9.0 0.0 7.0 0.0
CheeseGruyere 117.0 0.0 9.0 0.0 8.0 0.0
CheeseMuenster 486.0 1.0 40.0 1.0 31.0 0.0
CheeseCroissant 117.0 13.0 6.0 3.0 3.0 1.0
CheeseRoquefort 104.0 1.0 9.0 0.0 6.0 0.0
CheeseParmesan 21.0 0.0 1.0 0.0 2.0 0.0
CheeseMozarella 94.0 0.0 7.0 0.0 7.0 0.0
CottageCheese 96.0 2.0 4.0 0.0 13.0 0.0
YogurtPlainWholeMilk 149.0 11.0 8.0 11.0 9.0 0.0
FrozenYogurtChocolate 221.0 38.0 6.0 33.0 5.0 4.0