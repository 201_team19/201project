package csci201;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import sun.security.jca.GetInstance.Instance;

public class ClassParser extends JFrame{
	JTextField name, calorie, carb, fat, sugar, protein, fiber;
	JLabel nameLabel, calorieLabel, carbLabel, fatLabel, sugarLabel, proteinLabel, fiberLabel;
	ArrayList<FoodItem> database;
	ArrayList<Component> frameStuff;
	JPanel jp;
	JButton addButton, saveButton;
	JDBCDriver jdbcDriver;
	
	public ClassParser(){
		jdbcDriver = new JDBCDriver();
		setSize(300,500);
		setLocation(200,200);
		database = new ArrayList<FoodItem>();
		//read("resources/database.txt");
		createGUI();
		addEvents();
		setVisible(true);
	}

	private void createGUI() {
		frameStuff = new ArrayList<Component>();
		jp = new JPanel();
		jp.setLayout(new BoxLayout(jp, BoxLayout.Y_AXIS));
		name = new JTextField("Name");
		nameLabel = new JLabel("Name: ");
		frameStuff.add(nameLabel);
		frameStuff.add(name);
		name.setPreferredSize( new Dimension( 200, 24 ) );
		
		calorie = new JTextField("0");
		calorieLabel = new JLabel("Calories: ");
		frameStuff.add(calorieLabel);
		frameStuff.add(calorie);
		calorie.setPreferredSize( new Dimension( 200, 24 ) );
		
		fat = new JTextField("0");
		fatLabel = new JLabel("Fats: ");
		frameStuff.add(fatLabel);
		frameStuff.add(fat);
		fat.setPreferredSize( new Dimension( 200, 24 ) );
		
		
		carb = new JTextField("0");
		carbLabel = new JLabel("Carbs: ");
		frameStuff.add(carbLabel);
		frameStuff.add(carb);
		carb.setPreferredSize( new Dimension( 200, 24 ) );
		

		fiber = new JTextField("0");
		fiberLabel = new JLabel("Fibers: ");
		frameStuff.add(fiberLabel);
		frameStuff.add(fiber);
		fiber.setPreferredSize( new Dimension( 200, 24 ) );
		
		sugar = new JTextField("0");
		sugarLabel = new JLabel("Sugars: ");
		frameStuff.add(sugarLabel);
		frameStuff.add(sugar);
		sugar.setPreferredSize( new Dimension( 200, 24 ) );
		
		protein = new JTextField("0");
		proteinLabel = new JLabel("Protens: ");
		frameStuff.add(proteinLabel);
		frameStuff.add(protein);
		protein.setPreferredSize( new Dimension( 200, 24 ) );
		
		
		for(int i = 0; i < frameStuff.size(); i++){
			JPanel jp1 = new JPanel();
			jp1.setLayout(new FlowLayout());
			jp1.add(frameStuff.get(i));
			jp1.add(frameStuff.get(i+1));
			jp.add(jp1);
			i++;
		}
		addButton = new JButton("add");
		saveButton= new JButton("save");
		jp.add(addButton);
		jp.add(saveButton);
		add(jp);
		
	}
	
	private void addEvents(){
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name1 = name.getText();
				Double calories = Double.parseDouble(calorie.getText());
				Double carbs = Double.parseDouble(carb.getText());
				Double fats = Double.parseDouble(fat.getText());
				Double sugars = Double.parseDouble(sugar.getText());
				Double proteins = Double.parseDouble(protein.getText());
				Double fibers = Double.parseDouble(fiber.getText());
				 FoodItem tempFood = new FoodItem (name1, calories, carbs, fats, sugars, proteins, fibers);
				addToDatabase(tempFood);
				 resetLabels();
			}

		});
		saveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//write();
			}

		});
		this.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		       // write();
		        System.exit(0);
		    }
		});
	}
	
	private void resetLabels(){
		name.setText("Name ");
		calorie.setText("0");
		carb.setText("0");
		fat.setText("0");
		sugar.setText("0");
		protein.setText("0");
		fiber.setText("0");
	}

	
	
	private void addToDatabase(FoodItem tempFood) {
		jdbcDriver.addToDatabase(tempFood);
	}

	
	public static void main (String args[]){
		ClassParser cp = new ClassParser();
	}
}
