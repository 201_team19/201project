package csci201;

import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Stack;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class NewsFeedPanel extends JPanel {
	JPanel postHolder;
	JScrollPane jScrollPane; 
	Stack<JLabel> stackOfNewNews;
	
	{
		initializeComponents();
		add(jScrollPane);
	}
	void initializeComponents(){
		postHolder = new JPanel();
		postHolder.setLayout(new BoxLayout(postHolder, BoxLayout.Y_AXIS));
		jScrollPane = new JScrollPane(postHolder);
		jScrollPane.setPreferredSize(new Dimension(300,500));
		stackOfNewNews = new Stack<JLabel> ();
	}
	
	void addANewsPost(String newsPostInformation){
		String timeStamp = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
		JLabel newPost = new JLabel(Constants.getUsernameFromNewsString(newsPostInformation)+
				    " ate "+Constants.getFoodNameFromNewsString(newsPostInformation) + " at " + Constants.getTwelveHourTime(timeStamp));
		stackOfNewNews.add(newPost);
		postHolder.removeAll();
		for(int i = stackOfNewNews.size()-1; i >=0; i--){
			postHolder.add(stackOfNewNews.get(i));
		}
//		postHolder.add(newPost);
		repaint();
		revalidate();
	}
	
	public static void main(String[] args){
		JFrame frame = new JFrame();
		NewsFeedPanel nfp = new NewsFeedPanel();
		for(int i = 0; i < 100; i++){
			nfp.addANewsPost(Constants.getNewsString("courtney", "Apple Crisp"));
		}
		
		frame.add(nfp);
		//frame.add(new JLabel("hey"));
		frame.setVisible(true);
		frame.setSize(300, 500);
	}
	

}

