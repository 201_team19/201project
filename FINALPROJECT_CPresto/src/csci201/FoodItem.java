package csci201;

public class FoodItem {
	String name;
	double calories, carbs,fats, sugars, proteins, fibers;
	public FoodItem(String name, double calories2, double carbs2, double fats2, double sugars2, double proteins2, double fibers2){
		this.name = name;
		calories = calories2;
		carbs = carbs2;
		fats = fats2;
		sugars = sugars2;
		proteins = proteins2;
		fibers = fibers2; 
	}
	String getString(){
		String tempstring ="\n" + name  + " " + calories + " " + carbs + " " + fats + " " + sugars + " " + proteins + " " + fibers;
		return tempstring;
	}
	String getName(){
		return name;
	}
	double getCalories(){
		return calories;
	}
	double getCarbs(){
		return carbs;
	}
	double getFats(){
		return fats;
	}
	double getSugars(){
		return sugars;
	}
	double getProtein(){
		return proteins;
	}
	double getFiber(){
		return fibers;
	}
}
