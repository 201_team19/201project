package csci201;

public class Constants {

	//NEWS "username" "foodname" 
	static String getUsernameFromNewsString(String news){
		String[] newsARR = news.split("\\s+");
		return  newsARR[1];
	}
	
	static String getFoodNameFromNewsString(String news){
		String[] newsARR = news.split("\\s+");
		String temp = "";
		for(int i = 2; i < newsARR.length; i++){
			temp += newsARR[i]+ " ";
		}
		return temp; 
	}
	
	static String getNewsString(String userName, String foodName){
		String temp = "NEWS  " + userName + " " + foodName;
		return temp;
	}
	
	static String getTwelveHourTime(String time){
		String[] timeARR = time.split(":");
		String newTime = "";
			int timeInt = Integer.parseInt(timeARR[0]);
			if(timeInt >= 13){
				timeInt = timeInt - 12;
			}
			timeARR[0] = "" + timeInt;
		
		for(int i = 0; i < timeARR.length; i++){
			newTime += timeARR[i] ;
			if(i != timeARR.length-1){
				newTime += ":";
			}
		}
		return newTime;

	}
	
	
}
