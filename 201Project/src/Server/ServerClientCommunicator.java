package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerClientCommunicator extends Thread{
	Socket socket;
	ServerListener serverListener;
	private BufferedReader br;
	private PrintWriter pw;
	
	public ServerClientCommunicator(Socket s, ServerListener sl) throws IOException{
		socket = s;
		serverListener = sl;
		this.pw = new PrintWriter(socket.getOutputStream());
		this.br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	}
	
	public void sendMessage(String message){
		pw.println(message);
		pw.flush();
	}
	
	public void run(){
		try {
			String line = br.readLine();
			while(line != null){
				/*String [] message = line.split(" ",2);
				String messageType = message[0];*/
				/*switch(messageType){
					case "chatmessage":
						serverListener.sendMessage(line);
				}*/
				
				serverListener.sendMessage(line);
				line = br.readLine();
			}
			
			if(line == null){
				serverListener.removeServerClientCommunicator(this);
				// include error message in constants
				System.out.println(socket.getInetAddress() + ":" + socket.getPort() + " - " + "Constants.clientDisconnected");
				try{
					socket.close();
				} catch (IOException ioe) {
					// print some message here
				}
			}
		} catch (IOException e) {
			serverListener.removeServerClientCommunicator(this);
			// include error message in constants
			System.out.println(socket.getInetAddress() + ":" + socket.getPort() + " - " + "Constants.clientDisconnected");
		}
	}
}
