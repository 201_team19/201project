package Server;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import Client.MainWindow;
//import utilities.Util;

public class ServerListener extends Thread{
	private ServerSocket ss;
	private Vector<ServerClientCommunicator> sccVector;
	private MainWindow mainWindow;
	
	public ServerListener(ServerSocket ss){
		this.ss = ss;
		sccVector = new Vector<ServerClientCommunicator>();
		mainWindow = new MainWindow();
	}
	
	public void removeServerClientCommunicator(ServerClientCommunicator scc) {
		sccVector.remove(scc);
	}
	
	public void sendMessage(String message){
		for(ServerClientCommunicator scc : sccVector){
			scc.sendMessage(message);
		}
	}
	
	public void run(){
		try {
			while(true) {
					Socket s = ss.accept();	
					try {
						System.out.println("huy");
						ServerClientCommunicator scc = new ServerClientCommunicator(s, this);
						scc.start();
						scc.sendMessage("start");
						sccVector.add(scc);
					} catch (IOException ioe) {
						ioe.printStackTrace();
					}
			}
		} catch(BindException be) {
			be.printStackTrace();
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
			if (ss != null) {
				try {
					ss.close();
				} catch (IOException ioe1) {
					ioe1.printStackTrace();
				}
			}
		}
	}

}
