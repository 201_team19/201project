package Server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.Driver;

public class MySQLDriver {
	private Connection con;
	private Statement stmt;
	private static String s;
	
	private static String selectName; 
	
	private static String updateBreak; 
	private static String updateLunch; 
	private static String updateDinner; 
	private static String updateSnack; 
	private static String updateWork;


	private static String addBreak;
	private static String addLunch;
	private static String addDinner;
	private static String addSnack;
	private static String addWork; 
	private static String createTable;
	
	public MySQLDriver(){
		try{
			new Driver(); 
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public void connect(String name){
		s = name;
		selectName = "SELECT * FROM " + s + " WHERE day=?";
		
		updateBreak= "UPDATE " + s + " SET breakfast=? WHERE day=?";
		updateLunch = "UPDATE " + s + " SET lunch=? WHERE day=?";
		updateDinner = "UPDATE " + s + " SET dinner=? WHERE day=?";
		updateSnack = "UPDATE " + s + " SET snack=? WHERE day=?";
		updateWork = "UPDATE " + s + " SET workout=? WHERE day=?";
		
		addBreak = "INSERT INTO " + s + "(day,breakfast) VALUES(?,?)";
		addLunch = "INSERT INTO " + s + "(day,lunch) VALUES(?,?)";
		addDinner = "INSERT INTO " + s + "(day,dinner) VALUES(?,?)";
		addSnack = "INSERT INTO " + s + "(day,snack) VALUES(?,?)";
		addWork = "INSERT INTO " + s + "(day,workout) VALUES(?,?)";
		createTable = "create table " + s + " ( "
				    + "   day VARCHAR(20) PRIMARY KEY, breakfast VARCHAR(20), lunch VARCHAR(20), dinner VARCHAR(20), "
				    + "   snack VARCHAR(20), workout VARCHAR(20) )";
		try{
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/users?user=root&password=#Ntgnixel15");
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public void createTable(){
		try{
			stmt = con.createStatement();
			stmt.executeUpdate(createTable);
		}
		catch(SQLException se){
			se.printStackTrace();
		}
	}
	public void stop(){
		try{
			con.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public String checkBreak(String date){
		try{
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, date);
			ResultSet result = ps.executeQuery();
			while(result.next()){
				return result.getString(2);
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	public String checkLunch(String date){
		try{
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, date);
			ResultSet result = ps.executeQuery();
			while(result.next()){
				return result.getString(3);
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	public String checkDinner(String date){
		try{
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, date);
			ResultSet result = ps.executeQuery();
			while(result.next()){
				return result.getString(4);
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	public String checkWork(String date){
		try{
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, date);
			ResultSet result = ps.executeQuery();
			while(result.next()){
				return result.getString(6);
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	public String checkSnack(String date){
		try{
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, date);
			ResultSet result = ps.executeQuery();
			while(result.next()){
				return result.getString(5);
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	public boolean doesExist(String date){
		try{
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, date);
			ResultSet result = ps.executeQuery();
			while(result.next()){
				return true;
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		//System.out.println("Unable to find product with name: " + date);
		return false;
	}
	public void addBreak(String food, String date){
		try{
			PreparedStatement ps = con.prepareStatement(addBreak);
			ps.setString(1, date);
			ps.setString(2, food);
			ps.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public void addLunch(String food, String date){
		try{
			PreparedStatement ps = con.prepareStatement(addLunch);
			ps.setString(1, date);
			ps.setString(2, food);
			ps.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public void addDinner(String food, String date){
		try{
			PreparedStatement ps = con.prepareStatement(addDinner);
			ps.setString(1, date);
			ps.setString(2, food);
			ps.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public void addWorkout(String food, String date){
		try{
			PreparedStatement ps = con.prepareStatement(addWork);
			ps.setString(1, date);
			ps.setString(2, food);
			ps.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public void addSnack(String food, String date){
		try{
			PreparedStatement ps = con.prepareStatement(addSnack);
			ps.setString(1, date);
			ps.setString(2, food);
			ps.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public void updateBreak(String food, String date){
		try{
			PreparedStatement ps = con.prepareStatement(updateBreak);
			ps.setString(2, date);
			ps.setString(1, food);
			ps.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public void updateLunch(String food, String date){
		try{
			PreparedStatement ps = con.prepareStatement(updateLunch);
			ps.setString(2, date);
			ps.setString(1, food);
			ps.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public void updateDinner(String food, String date){
		try{
			PreparedStatement ps = con.prepareStatement(updateDinner);
			ps.setString(2, date);
			ps.setString(1, food);
			ps.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public void updateWorkout(String food, String date){
		try{
			PreparedStatement ps = con.prepareStatement(updateWork);
			ps.setString(2, date);
			ps.setString(1, food);
			ps.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public void updateSnack(String food, String date){
		try{
			PreparedStatement ps = con.prepareStatement(updateSnack);
			ps.setString(2, date);
			ps.setString(1, food);
			ps.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	/*public void update(String prodName){
		try{
			PreparedStatement ps = con.prepareStatement(updateProduct);
			ps.setString(2, prodName);
			ps.setInt(1, 1);
			ps.executeUpdate();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void add(String productNames){
		try{
			PreparedStatement ps = con.prepareStatement(addProduct);
			ps.setString(1, productNames);
			ps.setInt(2, 0);
			ps.executeUpdate();
			System.out.println("Adding product:" + productNames+ " to table with count 0");
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}*/
}
