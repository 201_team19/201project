package Server;

import java.net.ServerSocket;

public class Server {
	private ServerSocket ss;
	private static ServerListener serverListener;
	
	public Server(){
		PortPanel pp = new PortPanel();
		ss = pp.getServerSocket();
		listenForConnections();
	}
	
	private void listenForConnections() {
		serverListener = new ServerListener(ss);
		serverListener.start();
	}
	
	public static void main(String [] args) {
		new Server();
	}
}
