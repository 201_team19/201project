package Server;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.*;


public class PortPanel extends JFrame{
	private static final long serialVersionUID = 1L;
	private JTextField portTF;
	private JLabel descriptionLabel, portLabel, errorLabel;
	private JButton startButton;
	private Lock portLock;
	private Condition portCondition;
	private JPanel portJP;
	private ServerSocket ss;

	public PortPanel() {
		super("Server");
		setSize(300,175);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		initializeVariables();
		createGUI();
		addActionAdapters();
		setVisible(true);
	}
	
	private void initializeVariables() {
		descriptionLabel = new JLabel("<html>Enter the port number on which<br/>you would like the server to listen.</html>");
		portLabel = new JLabel("Port");
		errorLabel = new JLabel();
		portTF = new JTextField(20);
		portTF.setText("6789");
		startButton = new JButton("Start Listening");
		portLock = new ReentrantLock();
		portCondition = portLock.newCondition();
		portJP = new JPanel();
		//ss = null;
	}
	
	private void createGUI() {
		GridLayout gl = new GridLayout(4, 1);
		setLayout(gl);
		
		descriptionLabel.setHorizontalAlignment(JLabel.CENTER);
		add(descriptionLabel);
		
		portJP = new JPanel();
		portJP.add(portLabel);
		portJP.add(portTF);
		
		add(errorLabel);
		add(portJP);
		add(startButton);
	}
	
	private void addActionAdapters(){
		class PortListener implements ActionListener {
			public void actionPerformed(ActionEvent ae) {
				String portStr = portTF.getText();
				int portNumber = -1;
				try {
					portNumber = Integer.parseInt(portStr);
				} catch (Exception e) {
					errorLabel.setText("Constants.portErrorString");
					return;
				}
				if (portNumber > 0 && portNumber < 65535) {
					try {
						ServerSocket tempss = new ServerSocket(portNumber);
						portLock.lock();
						ss = tempss;
						portCondition.signal();
						portLock.unlock();						
					} catch (IOException ioe) {
						// this will get thrown if I can't bind to portNumber
						errorLabel.setText("Constants.portAlreadyInUseString");
					}
				}
				else {
					errorLabel.setText("Constants.portErrorString");
					return;
				}
			}
		}
		startButton.addActionListener(new PortListener());
		portTF.addActionListener(new PortListener());
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
	}
	
	public ServerSocket getServerSocket(){
		while (ss == null) {
			portLock.lock();
			try {
				portCondition.await();
			} catch (InterruptedException ie) {
				ie.printStackTrace();;
			}
			portLock.unlock();
		}
		return ss;
	}
}
