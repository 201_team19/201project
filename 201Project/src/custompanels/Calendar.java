package custompanels;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Calendar extends JFrame{
	
	private JComboBox monthSelect;
	private JComboBox daySelect;
	private JComboBox yearSelect;
	private JButton OkButton;
	private JPanel Selection;
	private final String[] months = {"January", "February", "March",
									"April", "May", "June", "July",
									"August", "September", "October",
									"November", "December"};
	private final String[] years = {"1986", "1987", "1988", "1989",
									"1990", "1991", "1992", "1993",
									"1994", "1995", "1996", "1997",
									"1998", "1999", "2000", "2001",
									"2002", "2003", "2004", "2005",
									"2006", "2007", "2008", "2009",
									"2010", "2011", "2012", "2013",
									"2014", "2015"};
	private final String[] days31 = {"1", "2", "3", "4", "5", "6",
									"7", "8", "9", "10", "11", "12",
									"13", "14", "15", "16", "17", "18",
									"19", "20", "21", "22", "23", "24",
									"25", "26", "27", "28", "29", "30", "31"};
	private final String[] days30 = {"1", "2", "3", "4", "5", "6",
									"7", "8", "9", "10", "11", "12",
									"13", "14", "15", "16", "17", "18",
									"19", "20", "21", "22", "23", "24",
									"25", "26", "27", "28", "29", "30"};
	private final String[] days28 = {"1", "2", "3", "4", "5", "6",
									"7", "8", "9", "10", "11", "12",
									"13", "14", "15", "16", "17", "18",
									"19", "20", "21", "22", "23", "24",
									"25", "26", "27", "28"};
	private java.util.Date date = new java.util.Date();
	private final int currentMonth = date.getMonth();
	private final int currentDay = date.getDate();
	private final int currentYear = date.getYear()-85;
	
	private String selectedMonth;
	private String selectedDay;
	private String selectedYear;
	
	public Calendar(){
		super("Choose Date");
		initializeVariables();
		createGUI();
		
		monthSelect.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				updateMonth((String)monthSelect.getSelectedItem());
			}
				
		});
			
		daySelect.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				updateDay((String)daySelect.getSelectedItem());
			}
				
		});
			
		yearSelect.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				updateYear((String)yearSelect.getSelectedItem());
			}
				
		});
	}
	
	public void initializeVariables(){
		monthSelect = new JComboBox(months);
		monthSelect.setSelectedIndex(currentMonth);
		daySelect = new JComboBox(days31);
		daySelect.setSelectedIndex(currentDay-1);
		yearSelect = new JComboBox(years);
		yearSelect.setSelectedIndex(currentYear-1);
		Selection = new JPanel();
		
		selectedMonth = (String)monthSelect.getSelectedItem();
		selectedDay = (String)daySelect.getSelectedItem();
		selectedYear = (String)yearSelect.getSelectedItem();
		
		OkButton = new JButton("OK");
		OkButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
			
		});
		
	}
	
	public void createGUI(){
		setSize(300, 250);
		setLocationRelativeTo(null);
		setLayout(new GridLayout(2, 1));
		Selection.setLayout(new FlowLayout());
		Selection.add(monthSelect);
		Selection.add(daySelect);
		Selection.add(yearSelect);
		add(Selection);
		add(OkButton);
	}
	
	public void updateMonth(String month){
		selectedMonth = month;
		adjustDays();
	}
	
	public void updateDay(String day){
		selectedDay = day;
	}
	
	public void updateYear(String year){
		selectedYear = year;
	}
	
	public void adjustDays(){
		if(selectedMonth == "April" || selectedMonth == "June"
				|| selectedMonth == "September" || selectedMonth == "November"){
			DefaultComboBoxModel update = new DefaultComboBoxModel(days30);
			update.setSelectedItem(selectedDay);
			String day = selectedDay;
			daySelect.removeAllItems();
			daySelect.setModel(update);
			selectedDay = day;
		}
		else if(selectedMonth == "February"){
			DefaultComboBoxModel update = new DefaultComboBoxModel(days28);
			update.setSelectedItem(selectedDay);
			String day = selectedDay;
			daySelect.removeAllItems();
			daySelect.setModel(update);
			selectedDay = day;
		}
		else{
			DefaultComboBoxModel update = new DefaultComboBoxModel(days31);
			update.setSelectedItem(selectedDay);
			String day = selectedDay;
			daySelect.removeAllItems();
			daySelect.setModel(update);
			selectedDay = day;
		}
			
	}
	
	public String getDate(){
		String date = selectedMonth + " " + selectedDay + ", " + selectedYear;
		return date;
	}
	
	public String getMonth(){
		return selectedMonth;
	}
	
	public String getDay(){
		return selectedDay;
	}

	public String getYear(){
		return selectedYear;
	}
	
	public String previousDate(){
		int previousDate = Integer.parseInt(selectedDay) - 1;
		
		selectedDay = Integer.toString(previousDate);
		
		return selectedMonth + " " + selectedDay + ", " + selectedYear;
	}
	
	public String nextDate(){
		int forwardDate = Integer.parseInt(selectedDay) + 1;
		
		selectedDay = Integer.toString(forwardDate);
		
		return selectedMonth + " " + selectedDay + ", " + selectedYear;
	}

}
