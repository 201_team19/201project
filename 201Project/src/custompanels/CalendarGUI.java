package custompanels;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import Client.ClientListener;
import Server.MySQLDriver;

public class CalendarGUI extends JPanel{
	public static final long serialVersionUID = 1;
	private JPanel overall, top, center, centerTop;
	private JButton prev, next;
	private JLabel month;
	private GregorianCalendar cal;
	private int currentMonth, currentYear;
	private CalGrid cg;
	private CalendarGUI ca;
	private MySQLDriver drive;
	private String userName;
	private ClientListener clientListener;
	
	public CalendarGUI(String name, ClientListener clientListener){
		//super("Calendar");
		this.clientListener = clientListener;
		userName = name;
		drive = new MySQLDriver();
		drive.connect(name);
		initializeComponents();
		createGUI();
		addEvents();
	}
	private void initializeComponents(){
		ca = this;
		overall = new JPanel();
		setLayout(new BorderLayout());
		overall.setLayout(new BorderLayout());
		top = new JPanel();
		top.setLayout(new FlowLayout(FlowLayout.CENTER, 25, 5));
		center = new JPanel();
		center.setLayout(new BorderLayout());
		
		centerTop = new JPanel();
		centerTop.setLayout(new GridLayout(1, 7));
		
		String[] months = {"January", "February", "March", "April", "May", "June",
				"July", "August", "September", "October", "November", "December"};
		
		prev = new JButton("<");
		next = new JButton(">");
		
		cal = new GregorianCalendar();
		currentMonth = cal.get(GregorianCalendar.MONTH);
		currentYear = cal.get(GregorianCalendar.YEAR);

		String m = months[currentMonth];
		String y = String.valueOf(currentYear);
		String t = m + " " + y;

		month = new JLabel(t);
		month.setFont(new Font("Helvetica", Font.CENTER_BASELINE, 30));
		
	}
	private void createGUI(){
		
		top.add(prev);
		top.add(month);
		top.add(next);
		overall.add(top, BorderLayout.NORTH);
		
		setDays();
		
		cg = new CalGrid(currentYear, currentMonth+1, this, drive);
		center.add(centerTop, BorderLayout.NORTH);
		center.add(cg, BorderLayout.CENTER);
		overall.add(center, BorderLayout.CENTER);
		add(overall);
	}
	private void addEvents(){
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		String[] months = {"January", "February", "March", "April", "May", "June",
				"July", "August", "September", "October", "November", "December"};
		
		//change month to the next one
		next.addActionListener(new ActionListener()  {
			public void actionPerformed(ActionEvent ae){
				//if it is the last month of the year, go to the next year
				if(currentMonth == 11){
					currentYear += 1;
					currentMonth = 0;
					String m = months[currentMonth];
					String y = String.valueOf(currentYear);
					String s = m + " " + y;
					month.setText(s);
					
					center.removeAll();
					cg.update(currentMonth+1, currentYear);
					center.add(centerTop, BorderLayout.NORTH);
					center.add(cg, BorderLayout.CENTER);
					revalidate();
					repaint();
				}
				else{
					currentMonth++;
					String m = months[currentMonth];
					String y = String.valueOf(currentYear);
					String s = m + " " + y;
					month.setText(s);
					center.removeAll();
					
					cg.update(currentMonth+1, currentYear);
					center.add(centerTop, BorderLayout.NORTH);
					center.add(cg, BorderLayout.CENTER);
					revalidate();
					repaint();
				}
			}
		});
		
		//go to the previous month
		prev.addActionListener(new ActionListener()  {
			public void actionPerformed(ActionEvent ae){
				//if it is the first day of the month, change the year
				if(currentMonth == 0){
					currentYear -= 1;
					currentMonth = 11;
					String m = months[currentMonth];
					String y = String.valueOf(currentYear);
					String s = m + " " + y;
					month.setText(s);
					
					center.removeAll();
					cg.update(currentMonth+1, currentYear);
					center.add(centerTop, BorderLayout.NORTH);
					center.add(cg, BorderLayout.CENTER);
					revalidate();
					repaint();
				}
				else{
					currentMonth--;
					String m = months[currentMonth];
					String y = String.valueOf(currentYear);
					String s = m + " " + y;
					month.setText(s);
					
					cg.update(currentMonth + 1, currentYear);
					center.add(centerTop, BorderLayout.NORTH);
					center.add(cg, BorderLayout.CENTER);
					revalidate();
					repaint();
				}
			}
		});
		cg.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent me){
				int h = getHeight();
				int w = getWidth();
				int wid = me.getX();
				int hei = me.getY() + 25;
				int totRows = cg.getRows();
				int col = wid*7/w;
				int row = hei*totRows/h;
				
				int day = cg.findDay(row, col);
				//for specified day, month, year, get the stuff the person has to show up
				String m = months[currentMonth];
				String y = String.valueOf(currentYear);
				String t = m + " " + day + ", " + y;
				int yy = currentYear - 2000;
				String yearStr = String.valueOf(yy);
				String mStr = "";
				if(currentMonth+1 < 10){
					mStr = "0" + currentMonth+1;
				}
				else{
					mStr = String.valueOf(currentMonth+1);
				}
				String dayStr = "";
				if(day < 10){
					dayStr = "0" + day;
				}
				else{
					dayStr = String.valueOf(day);
				}
				String tot = mStr + dayStr + yearStr;
				JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(ca);
				JDiaCal log = new JDiaCal(topFrame, t, drive, tot, ca);
				log.setVisible(true);
				
			}
		});
	}
	public void update(){
		String[] months = {"January", "February", "March", "April", "May", "June",
				"July", "August", "September", "October", "November", "December"};
		String m = months[currentMonth];
		String y = String.valueOf(currentYear);
		String s = m + " " + y;
		month.setText(s);
		
		cg.update(currentMonth + 1, currentYear);
		center.add(centerTop, BorderLayout.NORTH);
		center.add(cg, BorderLayout.CENTER);
		revalidate();
		repaint();
	}
	private void setDays(){
		String[] days = {"Sun", "Mon", "Tues", "Wed", "Th", "Fri", "Sat"};
		for(int i = 0; i < 7; i ++){
			JLabel day = new JLabel(days[i], SwingConstants.CENTER);
			centerTop.add(day);
		}
	}
	
	public void setName(String name){
		userName = name;
	}
	
	public void sendToServer(String newsString){
		clientListener.sendNewsString(newsString);
	}
	
	/*public static void main(String[] args){
		CalendarGUI cg = new CalendarGUI();
		cg.setVisible(true);
	}*/
}
