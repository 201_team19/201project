package custompanels;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MealCard extends JPanel{
	private int mealNum;
	private JLabel mealNumLabel;
	private JPanel mealTitle;
	private JLabel mealLabel;
	private JLabel timeStamp;
	private String time;
	private JLabel calories;
	private JPanel options;
	private JButton add;
	private JButton delete;
	private FoodCard _food;
	
	public MealCard(int numMeals, FoodCard food){
		mealNum = numMeals;
		_food = food;
		initializeVariables();
		createGUI();
	}
	
	public void initializeVariables(){
		mealNumLabel = new JLabel(ordinal(mealNum + 1));
		
		java.util.Date date = new java.util.Date();
		if(date.getMinutes() < 10)
			time = date.getHours()+":"+0+date.getMinutes();
		else
			time = date.getHours()+":"+date.getMinutes();
		timeStamp = new JLabel(time);
		
		mealTitle = new JPanel();
		
		mealLabel = new JLabel("Meal");
		
		//update according to getCalories function
		calories = new JLabel(410 + " calories");
		
		options = new JPanel();
		add = new JButton("Add");
		delete = new JButton("Delete");
		actionListeners();
	}
	
	public void createGUI(){
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		mealTitle.setLayout(new FlowLayout());
		mealTitle.add(mealNumLabel);
		mealTitle.add(mealLabel);
		mealTitle.add(timeStamp);
		mealTitle.add(calories);
		add(mealTitle);
		
		options.setLayout(new FlowLayout());
		options.add(add);
		options.add(delete);
		add(options);
	}
	
	public String ordinal(int i){
		int mod100 = i % 100;
		int mod10 = i % 10;
		if(mod10 == 1 && mod100 != 11)
			return i + "st";
		else if(mod10 == 2 && mod100 != 12)
			return i + "nd";
		else if(mod10 == 3 && mod100 != 13)
			return i + "rd";
		else
			return i + "th";
	}
	
	public void actionListeners(){
		add.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				add();
			}
			
		});
		delete.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				remove();
			}
			
		});
	}
	
	private void add(){
		removeAll();
		createGUI();
		add(_food);
		revalidate();
	}
	
	private void remove(){
		removeAll();
		revalidate();
		repaint();
	}
}
