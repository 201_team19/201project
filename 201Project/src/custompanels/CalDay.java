package custompanels;

import java.awt.Graphics;

import javax.swing.JPanel;

import Server.MySQLDriver;

//need to include a file, so I can extract data
//writes the name on each individual panel, should be used with mysql
public class CalDay extends JPanel{
	public static final long serialVersionUID = 1;
	private String d;
	private MySQLDriver driver;
	private boolean breakfast,lunch,dinner,workout,snack;
	
	public CalDay(int day, int month, int year, MySQLDriver dr){
		super();
		driver = dr;
		breakfast = false;
		lunch = false;
		dinner = false;
		snack = false;
		workout = false;
		int yy = year - 2000;
        String yearStr = String.valueOf(yy);
        String monthStr = "";
        if(month < 10){
        	monthStr = "0" + month;
        }
        else{
        	monthStr = String.valueOf(month);
        }
		String dayStr = "";
		if(day < 10){
			dayStr = "0" + day;
		}
		else{
			dayStr = String.valueOf(day);
		}
		String tot = monthStr + dayStr + yearStr;
		if(day == 0){
			d = " ";
		}
		else{
			d = String.valueOf(day);
			String s = "";
			if((s = driver.checkBreak(tot))!= null){
				breakfast = true;
			}
			if((s = driver.checkLunch(tot)) != null){
				lunch = true;
			}
			if((s = driver.checkDinner(tot)) != null){
				dinner = true;
			}
			if((s = driver.checkSnack(tot)) != null){
				snack = true;
			}
			if((s = driver.checkWork(tot)) != null){
				workout = true;
			}
		}
	}
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawString(d,5*getWidth()/6, getHeight()/5);
		//g.drawString("*B", getWidth()/6, getHeight()/3);
		if(breakfast){
			g.drawString("*B", getWidth()/6, getHeight()/5);
		}
		if(lunch){
			g.drawString("*L", getWidth()/6, getHeight()/2);
		}
		if(dinner){
			g.drawString("*D", getWidth()/6, 4*getHeight()/5);
		}
		if(snack){
			g.drawString("*S", 3*getWidth()/6, getHeight()/2);
		}
		if(workout){
			g.drawString("*W", 3*getWidth()/6, 4*getHeight()/5);
		}
	}
}
