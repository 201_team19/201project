package custompanels;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;

public class DatePanel extends JPanel{
	
	private JButton back;
	private JButton forward;
	private JButton today;
	private Calendar dateSelection;
	private String selectedDate;
	private String selectedMonth;
	private String selectedDay;
	private String selectedYear;
	
	public DatePanel(){
		
		initializeVariables();
		createGUI();
		
	}
	
	public void initializeVariables(){
		back = new JButton("Back");
		today = new JButton("Today");
		forward = new JButton("Forward");
		dateSelection = new Calendar();
		today.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				dateSelection.setVisible(true);
			}
			
		});
		
		actionListeners();
		
	}
	
	public void actionListeners(){
		
		back.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				String previousDate = dateSelection.previousDate();
				selectedDate = previousDate;
				refreshComponents();
			}
		});
		
		forward.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				String forwardDate = dateSelection.nextDate();
				selectedDate = forwardDate;
				refreshComponents();
			}
			
		});

		dateSelection.addWindowListener(new WindowListener(){

			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowClosed(WindowEvent e) {
				selectedDate = dateSelection.getDate();
				selectedMonth = dateSelection.getMonth();
				selectedDay = dateSelection.getDay();
				selectedYear = dateSelection.getYear();
				
				refreshComponents();
			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
	}
	
	public void refreshComponents(){
		
		today.setText(selectedDate);
		today.repaint();
		today.revalidate();
		
	}
	
	public void createGUI(){
		
		setLayout(new FlowLayout());
		add(back);
		add(today);
		add(forward);
		
	}
}
