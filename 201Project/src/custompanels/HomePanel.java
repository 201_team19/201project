package custompanels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

public class HomePanel extends JPanel{
	private static final long serialVersionUID = 1168261378804508991L;
	
	//datePanel
	private DatePanel dp;
	
	//calorie counter
	private CalorieCounter cc;
	
	private JPanel addMealPanel;
	private JButton addMealButton;
	
	//mealcard[s]
	private MealCard meal;
	private Vector<MealCard> meals;
	//foodcard
	private FoodCard food;
	
	//nutrition
	private NutritionPanel np;
	
	private Border raisedetched;
	
	//test
	class User{
		
	}
	
	class Chicken{
		String foodName = "Chicken";
		int calories = 400;
		public String getName(){
			return foodName;
		}
		public int getCalories(){
			return calories;
		}
	}
	
	public HomePanel(){
		
		initializeVariables();
		createGUI();

	}
	
	private void initializeVariables(){
		
		meals = new Vector<MealCard>();
		
		raisedetched = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
		
		dp = new DatePanel();
		
		cc = new CalorieCounter();
		cc.setBorder(raisedetched);
		
		addMealPanel = new JPanel();
		addMealButton = new JButton("Add Meal!");
		
		np = new NutritionPanel();
		
		//test
		Chicken c = new Chicken();
		food = new FoodCard(c.getName(), c.getCalories());
		meal = new MealCard(0, food);
		meal.setBorder(raisedetched);
		
		actionListeners();

	}
	
	private void refreshComponents(Vector<MealCard> meals){
		removeAll();
		add(dp);
		add(cc);
		for(int i = 0; i < meals.size(); i++)
			add(meals.get(i));
		addMealPanel.add(addMealButton);
		add(addMealPanel);
		
		add(np);
		
	}
	
	private void createGUI(){
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		//Date
		add(dp);
		
		//Calorie Counter
		add(cc);
		
		addMealPanel.add(addMealButton);
		add(addMealPanel);
		
		//Nutrition graph
		add(np);
		
	}
	
	private void actionListeners(){
		addMealButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				MealCard newMeal = new MealCard(meals.size(), food);
				newMeal.setBorder(raisedetched);
				meals.addElement(newMeal);
				refreshComponents(meals);	
			}
			
		});
	}
	
}
