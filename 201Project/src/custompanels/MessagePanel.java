package custompanels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;
import javax.swing.text.StyledDocument;

public class MessagePanel extends JPanel {
	private static final long serialVersionUID = -2108487730520497989L;
	private JTextArea chatTextArea, userListTextArea;
	private JTextField chatTextField;
	private JButton sendButton;
	private JScrollPane jp, jp1;
	private JPanel inputPanel;
	private final ActionListener sendAction;
	private Vector<String> onlineUsers;
	
	public MessagePanel(ActionListener sendAction){
		this.sendAction = sendAction;
		setLayout(new BorderLayout());
		initializeVariables();
		createGUI();
	}
	
	public void initializeVariables(){
		chatTextArea = new JTextArea();
		jp = new JScrollPane(chatTextArea);
		
		userListTextArea = new JTextArea();
		jp1 = new JScrollPane(userListTextArea);
		
		chatTextField = new JTextField();
		sendButton = new JButton("Send");
		sendButton.addActionListener(sendAction);
		
		inputPanel = new JPanel(new BorderLayout());
		
		onlineUsers = new Vector<String>();
	}
	
	public void createGUI(){
		inputPanel.add(sendButton, BorderLayout.EAST);
		inputPanel.add(chatTextField, BorderLayout.CENTER);
		
		add(inputPanel, BorderLayout.SOUTH);
		userListTextArea.setPreferredSize(new Dimension(80,getHeight()));
		add(jp, BorderLayout.CENTER);
		add(jp1, BorderLayout.WEST);
		
		chatTextArea.setEditable(false);
		chatTextArea.setCaretPosition(chatTextArea.getDocument().getLength());
		
		userListTextArea.setEditable(false);
		userListTextArea.setCaretPosition(chatTextArea.getDocument().getLength());
	}
	
	public void setText(String name, String message){
		chatTextArea.append(name + ": " + message + '\n');
		chatTextArea.setCaretPosition(chatTextArea.getDocument().getLength());
	}
	
	public void setExitText(String name){
		chatTextArea.append(name + " left the conversation" + '\n');
		chatTextArea.setCaretPosition(chatTextArea.getDocument().getLength());
	}
	
	public void addUser(String name){
		boolean contains = false;
		for(String uname : onlineUsers){
			if(uname.equals(name))
				contains = true;
		}
		
		if(!contains){
			onlineUsers.add(name);
			userListTextArea.append(name+'\n');
		}
	}
	
	public void removeUser(String name){
		for(String uname : onlineUsers){
			if(uname.equals(name))
				onlineUsers.remove(uname);
		}
		
		userListTextArea.setText(null);
		
		for(String uname : onlineUsers){
			userListTextArea.append(name+'\n');
		}
	}
	
	public String getUsers(){
		String users = null;
		for(String name : onlineUsers){
			users += name + " ";
		}
		return users;
	}
	
	public boolean isEmpty(){
		return onlineUsers.isEmpty();
	}
	
	public String getMessage(){
		return chatTextField.getText();
	}
	
	public void clearInput(){
		chatTextField.setText("");
	}
	
}