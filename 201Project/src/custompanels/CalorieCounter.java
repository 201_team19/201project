package custompanels;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class CalorieCounter extends JPanel{
	
	private JPanel calorieCounterLabels;
	private JLabel goalLabel;
	private JLabel foodLabel;
	private JLabel exerciseLabel;
	private JLabel remainingLabel;
	
	private JPanel calorieCounterData;
	private JLabel goalCLabel;
	private JLabel foodCLabel;
	private JLabel exerciseCLabel;
	private JLabel remainingCLabel;
	private JLabel minus;
	private JLabel plus;
	private JLabel equal;
	
	public CalorieCounter(){
		initializeVariables();
		createGUI();
	}
	
	public void initializeVariables(){
		
		calorieCounterLabels = new JPanel();
		goalLabel = new JLabel("Goal");
		foodLabel = new JLabel("Food");
		exerciseLabel = new JLabel("Exercise");
		remainingLabel = new JLabel("Remaining");
		
		calorieCounterData = new JPanel();
		
		//change according to getter functions
		goalCLabel = new JLabel("1985");
		foodCLabel = new JLabel("740");
		exerciseCLabel = new JLabel("140");
		remainingCLabel = new JLabel("1385");
		plus = new JLabel("+");
		minus = new JLabel("-");
		equal = new JLabel("=");
		
	}
	
	public void createGUI(){
		
		setLayout(new GridLayout(2, 1));
		
		calorieCounterLabels.setLayout(new FlowLayout());
		calorieCounterLabels.add(goalLabel);
		calorieCounterLabels.add(foodLabel);
		calorieCounterLabels.add(exerciseLabel);
		calorieCounterLabels.add(remainingLabel);
		add(calorieCounterLabels);
		
		calorieCounterData.setLayout(new FlowLayout());
		calorieCounterData.add(goalCLabel);
		calorieCounterData.add(minus);
		calorieCounterData.add(foodCLabel);
		calorieCounterData.add(plus);
		calorieCounterData.add(exerciseCLabel);
		calorieCounterData.add(equal);
		calorieCounterData.add(remainingCLabel);
		add(calorieCounterData);
		
	}

}
