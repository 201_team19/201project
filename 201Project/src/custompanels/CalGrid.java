package custompanels;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

import Server.MySQLDriver;

//sets up the grid for the month, with the help of calday, which actually writes the numbers
//on each individual jpanel
public class CalGrid extends JPanel{
	public static final long serialVersionUID = 1;
	private int month;
	private int year;
	private int rows, d;
	private MySQLDriver driver;
	
	CalGrid(int year, int month, CalendarGUI cagui, MySQLDriver drive){
		super();
		driver = drive;
		this.month = month;
		this.year = year;
		setUp();
	}
	private void setUp(){
		Border borderB = BorderFactory.createLineBorder(Color.BLACK, 1);
		int y = year - (14 - month) / 12;
        int x = y + y/4 - y/100 + y/400;
        int m = month + 12 * ((14 - month) / 12) - 2;
        d = (1 + x + (31*m)/12) % 7;
        int yy = year - 2000;
        String yearStr = String.valueOf(yy);
        String monthStr = "";
        if(month < 10){
        	monthStr = "0" + month;
        }
        else{
        	monthStr = String.valueOf(month);
        }
        
        
        boolean shorten = false;
        int[] days = {0, 31,28,31,30,31,30,31,31,30,31,30,31};
        boolean leap = false;
        if(month == 2)
        	leap = leapYear(year);
        if(leap){
        	days[month] = 29;
        }
       rows = 0;
        if(days[month]+d <= 35){
        	setLayout(new GridLayout(5,7));
        	rows = 5;
        	shorten = true;
        }
        else{
        	setLayout(new GridLayout(6,7));
        	rows = 6;
        }
        for(int i = 0; i < d; i ++){
        	//if(driver.doesExist(date))
        	CalDay day = new CalDay(0, month, year, driver);
        	day.setBorder(borderB); 
        	add(day);
        }
		for(int i = 1; i <= days[month]; i ++){
			
			CalDay day = new CalDay(i, month, year, driver);
			day.setBorder(borderB);
			add(day);
		}
		int tot = 42;
		if(shorten)
			tot = 35;
		for(int i = d + days[month]; i < tot; i ++){
			CalDay day = new CalDay(0, month, year, driver);
			day.setBorder(borderB);
			add(day);
		}
	}
	private boolean leapYear(int y){
		if((y % 4 == 0) && (y % 100 != 0))
			return true;
		if(y % 400 == 0)
			return true;
		return false;
	}
	public int getRows(){
		return rows;
	}
	public int findDay(int row, int col){
		int a = col - d + 1;
		int b = row*7;
		return a + b;
	}
	public void update(int month, int year){
		removeAll();
		this.month = month;
		this.year = year;
		setUp();
	}
	//JDBCDriver.searchFor(part of the word);
}
