package custompanels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class NutritionPanel extends JPanel{
	
	private PieChart nutrition;
	private JPanel nutritionLegend;
	private ArrayList<Color> colors;
	private JPanel carbs;
	private JPanel protein;
	private JPanel fat;
	private JPanel nutritionLabel;
	private JPanel carbsPanel;
	private JPanel proteinPanel;
	private JPanel fatPanel;
	private JLabel carbsLabel;
	private JLabel proteinLabel;
	private JLabel fatLabel;
	
	public NutritionPanel(){
		
		initializeVariables();
		createGUI();
		
	}
	
	public void initializeVariables(){
		
		nutrition = new PieChart();
		nutritionLegend = new JPanel();
		colors = new ArrayList<Color>();
		colors.add(Color.blue);
		colors.add(Color.red);
		colors.add(Color.pink);
		carbs = new JPanel();
		protein = new JPanel();
		fat = new JPanel();
		nutritionLabel = new JPanel();
		carbsPanel = new JPanel();
		proteinPanel = new JPanel();
		fatPanel = new JPanel();
		carbsLabel = new JLabel("Carbs");
		proteinLabel = new JLabel("Protein");
		fatLabel = new JLabel("Fat");
		
	}
	
	public void createGUI(){
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(nutrition);
		carbs.setPreferredSize(new Dimension(20, 20));
		carbs.setBackground(colors.get(0));
		protein.setPreferredSize(new Dimension(20, 20));
		protein.setBackground(colors.get(1));
		fat.setPreferredSize(new Dimension(20, 20));
		fat.setBackground(colors.get(2));
		nutritionLegend.setLayout(new BoxLayout(nutritionLegend, BoxLayout.Y_AXIS));
		nutritionLabel.setLayout(new FlowLayout());
		nutritionLabel.add(new JLabel("Macros"));
		nutritionLabel.add(new JLabel("Consumed"));
		nutritionLabel.add(new JLabel("Goal"));
		nutritionLegend.add(nutritionLabel);
		carbsPanel.setLayout(new FlowLayout());
		carbsPanel.add(carbs);
		carbsPanel.add(carbsLabel);
		carbsPanel.add(new JLabel("134 g or 58%"));
		carbsPanel.add(new JLabel("2%"));
		nutritionLegend.add(carbsPanel);
		proteinPanel.setLayout(new FlowLayout());
		proteinPanel.add(protein);
		proteinPanel.add(proteinLabel);
		proteinPanel.add(new JLabel("23 g or 22%"));
		proteinPanel.add(new JLabel("30%"));
		nutritionLegend.add(proteinPanel);
		fatPanel.setLayout(new FlowLayout());
		fatPanel.add(fat);
		fatPanel.add(fatLabel);
		fatPanel.add(new JLabel("46 g or 20%"));
		fatPanel.add(new JLabel("50%"));
		nutritionLegend.add(fatPanel);
		add(nutritionLegend);
		
	}

}
