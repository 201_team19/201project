package custompanels;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import App.FoodItem;
import custompanels.JAdd;
import App.JDBCDriver;

import Server.MySQLDriver;

//connect with a file, so all the info show up
//Pops up when you click on a date in the calendar gui/ calgrid, is where you can add meals
//to your week/see them
public class JDiaCal extends JDialog{
	public static final long serialVersionUID = 1;
	private JLabel search, breakfast, lunch, dinner, snacks, workout, calLabel;
	private JTextField textSearch;
	private JPanel row1, bFast, lun, din, work, snack;
	private JComboBox addList;
	private JDiaCal jdc;
	private String calorie, fats, carb, protein, date;
	private JLabel add;
	private String itemName;
	private MySQLDriver drive;
	private CalendarGUI ca;
	private String searchProd;
	private JDBCDriver jdbcDriver = new JDBCDriver();
	private String meal = "BREAKFAST";
	
	//Should show what the person already has down on their file
	JDiaCal(JFrame f, String s, MySQLDriver dr, String tot, CalendarGUI ca){
		super(f, s);
		date = tot;
		this.ca = ca;
		searchProd = "";
		drive = dr;
		initializeComponents();
		createGUI();
		addEvents();
	}
	private void initializeComponents(){
		setSize(350, 350);
		setLayout(new GridLayout(6, 1));
		jdc = this;
		calorie = " ";
		fats = " ";
		carb = " ";
		protein = " ";
		
		bFast = new JPanel();
		lun = new JPanel();
		din = new JPanel();
		snack = new JPanel();
		work = new JPanel();
		row1 = new JPanel();
		search = new JLabel("Enter");
		search.setFont(new Font("Helvetica", Font.CENTER_BASELINE, 18));
		textSearch = new JTextField(10);
		
		String[] addStrings = { "Add to Breakfast", "Add to Lunch", "Add to Dinner",
				"Add to Snacks", "Add to Workout" };

		//Create the combo box, select item at index 4.
		//Indices start at 0, so 4 specifies the pig.
		addList = new JComboBox(addStrings);
		//addList.setEnabled(false);
		/*petList.setSelectedIndex(4);
		petList.addActionListener(this);*/
		
		breakfast = new JLabel("Breakfast: ");
		breakfast.setFont(new Font("Helvetica", Font.CENTER_BASELINE, 14));
		lunch = new JLabel("Lunch: ");
		lunch.setFont(new Font("Helvetica", Font.CENTER_BASELINE, 14));
		dinner = new JLabel("Dinner: ");
		dinner.setFont(new Font("Helvetica", Font.CENTER_BASELINE, 14));
		snacks = new JLabel("Snacks: ");
		snacks.setFont(new Font("Helvetica", Font.CENTER_BASELINE, 14));
		workout = new JLabel("Workout: ");
		workout.setFont(new Font("Helvetica", Font.CENTER_BASELINE, 14));
	}
	private void createGUI(){
		String s;
		row1.add(search);
		row1.add(textSearch);
		row1.add(addList);
		add(row1);
		
		bFast.add(breakfast);
		if((s = drive.checkBreak(date))!= null){
			bFast.add(getCaloriesForLabel(s));
		}
		bFast.setLayout(new FlowLayout(FlowLayout.LEFT));
		add(bFast);
		
		lun.add(lunch);
		if((s = drive.checkLunch(date)) != null){
			
			lun.add(getCaloriesForLabel(s));
		}
		lun.setLayout(new FlowLayout(FlowLayout.LEFT));
		add(lun);
		
		din.add(dinner);
		if((s = drive.checkDinner(date)) != null){
			
			din.add(getCaloriesForLabel(s));
		}
		din.setLayout(new FlowLayout(FlowLayout.LEFT));
		add(din);
		
		snack.add(snacks);	
		if((s = drive.checkSnack(date)) != null){
			snack.add(getCaloriesForLabel(s));
		}
		snack.setLayout(new FlowLayout(FlowLayout.LEFT));
		add(snack);
		
		work.add(workout);
		if((s = drive.checkWork(date)) != null){
			JLabel lab = new JLabel(s);
			work.add(lab);
		}
		work.setLayout(new FlowLayout(FlowLayout.LEFT));
		add(work);
	}
	private void addEvents(){
		addList.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox)e.getSource();
		        itemName = (String)cb.getSelectedItem();
		        System.out.println(itemName);
		        boolean exists = false;
		        searchProd = textSearch.getText();
		        
		        Vector<FoodItem> food = jdbcDriver.searchFor(searchProd);
		        if(!food.isEmpty()){
		        	exists = true;
		        }
		        
		        add = new JLabel(searchProd + ("--"));
		        if(itemName.equals("Add to Workout")){
		        	JWork workIt = new JWork(jdc, searchProd);
		        	workIt.setVisible(true);
		        }
		        else{
			        JAdd dog = new JAdd(jdc, searchProd, exists, food);
			        dog.setVisible(true);
			        dog.setModal(true);
		        }
		        //if it is in our system, give nutritional value
		        //else
		        //name, calories, carbs, fats, sugar, protein, fiber
			}
		});
	}
	public void changeWork(){
		if(drive.doesExist(date))
    		drive.updateWorkout(searchProd, date);
    	else
    		drive.addWorkout(searchProd, date);
		work.removeAll();
    	work.add(workout);
    	work.add(add);
    	work.add(calLabel);
    	getContentPane().revalidate();
		getContentPane().repaint();
		ca.update();
	}
	public void setWorkCal(String s){
		calorie = s;
        calLabel = new JLabel("Calories = " + calorie);

	}
	
	public JLabel getCaloriesForLabel(String s){
		Vector<FoodItem> foods = jdbcDriver.searchFor(s);
		if(!foods.isEmpty()){
			double temp = foods.get(0).getCalories();
			s += "-- calories = " + temp;
		}
		JLabel lab = new JLabel(s);
		return lab;
	}
	//get the date from the table, if there is no date, create one and add in the specified meal
	public void changeScreen(){

        if(itemName.equals("Add to Breakfast")){
        	if(drive.doesExist(date))
        		drive.updateBreak(searchProd, date);
        	else
        		drive.addBreak(searchProd, date);
        	bFast.removeAll();
        	bFast.add(breakfast);
        	bFast.add(add);
        	bFast.add(calLabel);
        	meal = "BREAKFAST";
        	getContentPane().revalidate();
			getContentPane().repaint();
        }
        else if(itemName.equals("Add to Lunch")){
        	if(drive.doesExist(date))
        		drive.updateLunch(searchProd, date);
        	else
        		drive.addLunch(searchProd, date);
        	lun.removeAll();
        	lun.add(lunch);
        	lun.add(add);
        	lun.add(calLabel);
        	meal = "LUNCH";
        	getContentPane().revalidate();
			getContentPane().repaint();
        }
        else if(itemName.equals("Add to Dinner")){
        	if(drive.doesExist(date))
        		drive.updateDinner(searchProd, date);
        	else
        		drive.addDinner(searchProd, date);
        	din.removeAll();
        	din.add(dinner);
        	din.add(add);
        	din.add(calLabel);
        	meal = "DINNER";
        	getContentPane().revalidate();
			getContentPane().repaint();
        }
        else if(itemName.equals("Add to Snacks")){
        	if(drive.doesExist(date))
        		drive.updateSnack(searchProd, date);
        	else
        		drive.addSnack(searchProd, date);
	        snack.add(add);
        	snack.add(calLabel);
        	meal = "SNACKTIME";
        	getContentPane().revalidate();
			getContentPane().repaint();
        }
        ca.update();
	}
	public void setInfo(String cal, String fat, String carbs, String prot){
		calorie = cal;
        calLabel = new JLabel("Calories = " + calorie);
		System.out.println(calorie);
		fats = fat;
		carb = carbs;
		protein = prot;
	}
	
	public void sendToServer(String newsString){
		ca.sendToServer(newsString+ " " + meal);
	}
}
