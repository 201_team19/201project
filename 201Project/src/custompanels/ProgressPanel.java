package custompanels;

import java.awt.Dimension;

import javax.swing.*;

public class ProgressPanel extends JPanel{
	private static final long serialVersionUID = -2108487730520497989L;

	private JTabbedPane tabbedPane;
	
	public ProgressPanel(){
		initializeVariables();
		createGUI();
	}
	
	private void initializeVariables(){
		tabbedPane = new JTabbedPane();
	}
	
	private void createGUI(){
		tabbedPane.setPreferredSize(new Dimension(580,330));
		//temporary placeholder
		JPanel todayPanel = new JPanel();
		JLabel todayLabel = new JLabel("Today");
		todayPanel.add(todayLabel);
		tabbedPane.add("Today",todayPanel);
		
		JPanel weekPanel = new JPanel();
		JLabel weekLabel = new JLabel("This Week");
		weekPanel.add(weekLabel);
		tabbedPane.add("This week",weekPanel);
		
		JPanel alltimePanel = new JPanel();
		JLabel alltimeLabel = new JLabel("All-time");
		alltimePanel.add(alltimeLabel);
		tabbedPane.add("All-time",alltimePanel);
		
		add(tabbedPane);
	}
}