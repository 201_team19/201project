package custompanels;

import java.awt.BorderLayout;

import javax.swing.*;

import Client.ClientListener;

public class MealsPanel extends JPanel{
	private static final long serialVersionUID = -2108487730520497989L;
	CalendarGUI cg;
	ClientListener clientListener;
	
	public MealsPanel(ClientListener clientListener){
		this.clientListener = clientListener;
	}
	
	public void setUserName(String name){
		setLayout(new BorderLayout());
		cg = new CalendarGUI(name, clientListener);
		add(cg, BorderLayout.CENTER);
	}
}