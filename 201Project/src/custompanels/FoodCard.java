package custompanels;

import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FoodCard extends JPanel{
	
	private String foodName;
	private int foodCalories;
	
	private JLabel foodNameLabel;
	private JLabel foodCaloriesLabel;
	
	public FoodCard(String food, int calories){
		foodName = food;
		foodCalories = calories;
		
		initializeVariables();
		createGUI();
	}
	
	public void initializeVariables(){
		foodNameLabel = new JLabel(foodName);
		foodCaloriesLabel = new JLabel(Integer.toString(foodCalories));
	}
	
	public void createGUI(){
		setLayout(new FlowLayout());
		add(foodNameLabel);
		add(foodCaloriesLabel);
	}
	
}
