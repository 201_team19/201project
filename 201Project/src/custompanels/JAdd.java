package custompanels;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import App.FoodItem;
import App.JDBCDriver;
import custompanels.JDiaCal;

//pops up after you click add food from the jdropdown menu in jdiacal, can
//set the info on the food in this window
public class JAdd extends JDialog{

	public static final long serialVersionUID = 1;
	private JLabel calories, fat, carbs, protein, sugar, fiber;
	private JTextField calorieText, fatText, carbsText, proteinText, sugarText, fiberText;
	private JPanel caloriePan, fatPan, carbsPan, proteinPan, sugarPan, fiberPan;
	private JButton confirmButton;
	private boolean exists;
	private JLabel title;
	String cal, fats, carb, prot, sug, fib;
	private custompanels.JDiaCal jdial;
	private String foodStr;
	private JDBCDriver jdbcDriver = new JDBCDriver();
	private FoodItem food;
	private Vector<FoodItem> foodVect;
	private String protS, fatS, carbS, calS, sugS, fibS;
	
	
	public JAdd(custompanels.JDiaCal jdc, String s, boolean exists, Vector<FoodItem> f){
		super(jdc, s);
		jdial = jdc;
		foodStr = s;
		this.exists = exists;
		foodVect = f;
		initializeComponents();
		createGUI();
		addEvents();
	}
	//name, calories, carbs, fats, sugar, protein, fiber

//	public JAdd(JDiaCal jdc, String searchProd, boolean exists2) {
//		super(ca, s);
//		jdial = ca;
//		foodStr = s;
//		this.exists = exists;
//		foodVect = f;
//		initializeComponents();
//		createGUI();
//		addEvents();
//		}

	private void initializeComponents(){
		setSize(350, 300);
		setLayout(new GridLayout(8, 1));
		cal = " ";
		fats = " ";
		carb = " ";
		prot = " ";
		sug = " ";
		fib = " ";
		
		confirmButton = new JButton("Confirm");
		title = new JLabel("Fill in the info, if it's not already");
		title.setFont(new Font("Helvetica", Font.BOLD, 18));
		
		caloriePan = new JPanel();
		calories = new JLabel("Calories: ");
		calorieText = new JTextField(10);
		
		fatPan = new JPanel();
		fat = new JLabel("Fat:           ");
		fatText = new JTextField(10);
		
		carbsPan = new JPanel();
		carbs = new JLabel("Carbs:     ");
		carbsText = new JTextField(10);
		
		proteinPan = new JPanel();
		protein = new JLabel("Protein:   ");
		proteinText = new JTextField(10);
		
		sugarPan = new JPanel();
		sugar = new JLabel("Sugar:     ");
		sugarText = new JTextField(10);
		
		fiberPan = new JPanel();
		fiber = new JLabel("Fiber:      ");
		fiberText = new JTextField(10);
	}
	private void createGUI(){
		if(exists && !foodVect.isEmpty()){
			System.out.println("Hello");
			food = foodVect.elementAt(0);
			add(title);
			caloriePan.add(calories);
			double calFound = food.getCalories();
			calS = "" + calFound;
			JLabel calL = new JLabel(calS);
			caloriePan.add(calL);
			caloriePan.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(caloriePan);
			fatPan.add(fat);
			double fatFound = food.getCalories();
			fatS = "" + fatFound;
			JLabel fatL = new JLabel(fatS);
			fatPan.add(fatL);
			fatPan.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(fatPan);
			carbsPan.add(carbs);
			double carbFound = food.getCalories();
			carbS = "" + carbFound;
			JLabel carbL = new JLabel(carbS);
			carbsPan.add(carbL);
			carbsPan.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(carbsPan);
			proteinPan.add(protein);
			double protFound = food.getCalories();
			protS = "" + protFound;
			JLabel protL = new JLabel(protS);
			proteinPan.add(protL);
			proteinPan.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(proteinPan);
			sugarPan.add(sugar);
			double sugFound = food.getCalories();
			sugS = "" + sugFound;
			JLabel sugL = new JLabel(sugS);
			sugarPan.add(sugL);
			sugarPan.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(sugarPan);
			fiberPan.add(fiber);
			double fibFound = food.getCalories();
			fibS = "" + fibFound;
			JLabel fibL = new JLabel(fibS);
			fiberPan.add(fibL);
			fiberPan.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(fiberPan);
			JPanel last = new JPanel();
			last.setLayout(new FlowLayout(FlowLayout.LEFT));
			last.add(confirmButton);
			add(last);
		}
		else{
			add(title);
			caloriePan.add(calories);
			caloriePan.add(calorieText);
			caloriePan.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(caloriePan);
			fatPan.add(fat);
			fatPan.add(fatText);
			fatPan.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(fatPan);
			carbsPan.add(carbs);
			carbsPan.add(carbsText);
			carbsPan.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(carbsPan);
			proteinPan.add(protein);
			proteinPan.add(proteinText);
			proteinPan.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(proteinPan);
			sugarPan.add(sugar);
			sugarPan.add(sugarText);
			sugarPan.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(sugarPan);
			fiberPan.add(fiber);
			fiberPan.add(fiberText);
			fiberPan.setLayout(new FlowLayout(FlowLayout.LEFT));
			add(fiberPan);
			JPanel last = new JPanel();
			last.setLayout(new FlowLayout(FlowLayout.LEFT));
			last.add(confirmButton);
			add(last);
		}
	}
	private void addEvents(){
		confirmButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if(!exists){
					cal = calorieText.getText();
					fats = fatText.getText();
					carb = carbsText.getText();
					prot = proteinText.getText();
					sug = sugarText.getText();
					fib = fiberText.getText();
				}
				else{
					cal = calS;
					fats = fatS;
					carb = carbS;
					prot = protS;
					sug = sugS;
					fib = fibS;
				}
			

				double calorieD = Double.parseDouble(cal);
				double fatsD = Double.parseDouble(fats);
				double carbsD = Double.parseDouble(carb);
				double protD = Double.parseDouble(prot);
				double sugD = Double.parseDouble(sug);
				double fibD = Double.parseDouble(fib);
				if(!exists)
					jdbcDriver.addToDatabase(new FoodItem(foodStr, calorieD, carbsD, fatsD, sugD, protD, fibD));
				
				
				jdial.setInfo(cal, fats, carb, prot);
				jdial.changeScreen();
				
				jdial.sendToServer(foodStr);
				dispose();
			}
		});
	}
}
