package custompanels;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


//Used to add a workout, when you click Add a Workout on the jdropdown box for jdiacal
public class JWork extends JDialog{

	public static final long serialVersionUID = 1;
	private JButton confirmButton;
	private JPanel confirmPan, caloriePan;
	private JLabel calories, title;
	private JTextField calorieText;
	private JDiaCal jcal;
	
	JWork(JDiaCal ca, String s){
		super(ca, s);
		jcal = ca;
		initializeComponents();
		createGUI();
		addEvents();
	}
	private void initializeComponents(){
		setSize(350, 250);
		setLayout(new GridLayout(3, 1));
		title = new JLabel("Enter Number of Calories Burned for Workout");
		title.setFont(new Font("Helvetica", Font.BOLD, 18));
		caloriePan = new JPanel();
		calories = new JLabel("Calories Burned: ");
		calorieText = new JTextField(10);
		
		confirmButton = new JButton("Confirm");
		confirmPan = new JPanel();
		confirmPan.setLayout(new FlowLayout(FlowLayout.LEFT));
	}
	private void createGUI(){
		add(title);
		caloriePan.add(calories);
		caloriePan.add(calorieText);
		caloriePan.setLayout(new FlowLayout(FlowLayout.LEFT));
		add(caloriePan);
		confirmPan.add(confirmButton);
		add(confirmPan);
	}
	private void addEvents(){
		confirmButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				jcal.setWorkCal(calorieText.getText());
				jcal.changeWork();
				dispose();
			}
		});
	}
}
