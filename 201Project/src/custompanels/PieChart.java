package custompanels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class PieChart extends JPanel{
	
	ArrayList<Double> values = new ArrayList<Double>();
	
	ArrayList<Color> colors = new ArrayList<Color>();
	
	public PieChart(){
		values.add(new Double(10));
		values.add(new Double(25));
		values.add(new Double(65));
		
		colors.add(Color.blue);
		colors.add(Color.red);
		colors.add(Color.pink);
		
		setPreferredSize(new Dimension(225, 225));
		setMaximumSize(new Dimension(225, 225));
		setMinimumSize(new Dimension(50, 50));
	}
	
	@Override
	protected void paintComponent(Graphics g){
		
		int width = getSize().width;
		
		Graphics2D g2d = (Graphics2D)g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		
		int lastPoint = -270;
		
		for(int i = 0; i < values.size(); i++){
			g2d.setColor(colors.get(i));
			
			Double val = values.get(i);
			Double angle = (val/100)*360;
			
			g2d.fillArc(0, 0, width, width, lastPoint, -angle.intValue());
			
			lastPoint = lastPoint + -angle.intValue();
		}
	}

}
