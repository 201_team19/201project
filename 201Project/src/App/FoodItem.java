package App;

public class FoodItem {
	String name;
	double calories, carbs,fats, sugars, proteins, fibers;
	public FoodItem(String name, double calories2, double carbs2, double fats2, double sugars2, double proteins2, double fibers2){
		this.name = name;
		calories = calories2;
		carbs = carbs2;
		fats = fats2;
		sugars = sugars2;
		proteins = proteins2;
		fibers = fibers2; 
	}
	String getString(){
		String tempstring ="\n" + name  + " " + calories + " " + carbs + " " + fats + " " + sugars + " " + proteins + " " + fibers;
		return tempstring;
	}
	String getName(){
		return name;
	}
	public double getCalories(){
		return calories;
	}
	public double getCarbs(){
		return carbs;
	}
	public double getFats(){
		return fats;
	}
	public double getSugars(){
		return sugars;
	}
	public double getProtein(){
		return proteins;
	}
	public double getFiber(){
		return fibers;
	}
}
