package App;

import java.awt.Image;
import java.util.Vector;

public class User {
	
	private String _firstName;
	private String _lastName;
	private int _age;
	private String _gender;
	private int _weight;
	private int _height;
	private int _BMR;
	private Vector<User> friends;
	private Image _profImg;
	private Goals goals;
	private String username, password;
	
	
	public User(String username, String password,String name, int age,  int weight, int heightInInches, String gender){
		this.username = username;
		this.password = password;
		setName(name);
		_age = age;
		_weight = weight;
		_height = heightInInches;
		_gender = gender;
	}
	
	public User(){
		goals = new Goals();
	}	
	
	public void setUsername(String username){
		this.username = username;
	}
	public String getUsername(){
		return username;
	}
	
	public void setPassword(String password){
		this.password= password;
	}
	public String getPassword(){
		return this.password;
	}
	
	public String getName(){
		return _firstName+" "+_lastName;
	}
	
	public void setName(String firstName, String lastName){
		_firstName = firstName;
		_lastName = lastName;
	}
	
	public void setName(String fullName){
		String[] fullARR = fullName.split("\\s+");
		_firstName = fullARR[0];
		_lastName = fullARR[1];
	}
	
	public int getAge(){
		return _age;
	}	
	
	public void setAge(int age){
		_age = age;
	}
	
	public String getGender(){
		return _gender;
	}
	
	public void setGender(String gender){
		_gender = gender;
	}
	
	public int getWeight(){
		return _weight;
	}
	
	public void setWeight(int weight){
		_weight = weight;
	}
	
	public int getHeight(){
		return _height;
	}
	
	public void setHeight(int height){
		_height = height;
	}
	
	public int getBMR(){
		return _BMR;
	}
	
	public void setBMR(int BMR){
		_BMR = BMR;
	}
	
	public void setGoal(String goal){ //change according to input
		goals.setWeightGoal(goal);
	}
	
	public int getCalories(){
		return goals.getCalories();
	}
	
	public void addFriend(User user){
		friends.add(user);
	}
	
	public void removeFriend(User user){
		friends.remove(user);
	}
	
	public Vector<User> getFriends(User user){
		return friends;
	}
	
	public Image getProfImg(){
		return _profImg;
	}
	
	public void setProfImg(Image profImg){
		_profImg = profImg;
	}

}
