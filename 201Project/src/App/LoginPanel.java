//package Client;
package App;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;

import Client.MainWindow;
import Client.ClientListener;

import javax.swing.*;

public class LoginPanel extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private JPanel jp, firstJP, secondJP, thirdJP, errorJP;
	private JLabel usernameLabel, passwordLabel, errorLabel;
	private JTextField usernameTF, passwordTF;
	private JButton signInButton, signUpButton;
	private JDBCDriver jdbcDriver = new JDBCDriver(); 
	private ClientListener clientListener;
	private Socket socket;
	private LoginPanel loginPanel;
	
	public LoginPanel(Socket socket){
		super("Login");
		setSize(300,150);
		setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		
		this.socket = socket;
		loginPanel = this;
		
		createGUI();
		addActionAdapters();
		
		setVisible(true);
	}
	
	private void createGUI(){
		jp = new JPanel(new GridLayout(4,1));
		jp.setBorder(BorderFactory.createEmptyBorder(15,5,5,5));
		add(jp);
		
		firstJP = new JPanel();
		usernameLabel = new JLabel("Username:");
		usernameTF = new JTextField(15);
		firstJP.add(usernameLabel);
		firstJP.add(usernameTF);
		jp.add(firstJP);
		
		secondJP = new JPanel();
		passwordLabel = new JLabel("Password:");
		passwordTF = new JTextField(15);
		secondJP.add(passwordLabel);
		secondJP.add(passwordTF);
		jp.add(secondJP);
		
		thirdJP = new JPanel();
		signInButton = new JButton("Sign in");
		signUpButton = new JButton("Sign up");
		thirdJP.add(signInButton);
		thirdJP.add(signUpButton);
		jp.add(thirdJP);
		
		errorJP = new JPanel();
		errorLabel = new JLabel();
		errorJP.add(errorLabel);
		jp.add(errorJP);
	}
	
	private void addActionAdapters(){
		signInButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(jdbcDriver.containsLogIn(usernameTF.getText(), passwordTF.getText())){
					clientListener = new ClientListener(loginPanel, socket);
					clientListener.setUserName(usernameTF.getText());
					clientListener.openMainWindow();
					clientListener.sendMessage("username "+clientListener.getUserName());
					clientListener.getMainWindow().getMealsPanel().setUserName(clientListener.getUserName());

				}
				else{
					errorLabel.setText("Username Password combination not found");
					errorLabel.setForeground(Color.RED);
					repaint();
					revalidate();
				}
				
			}
		});	
		signUpButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				SignUpPanel sp = new SignUpPanel(socket, loginPanel);
			}
		});	
	}
	
	public void setClientListener(ClientListener cl){
		clientListener = cl;
	}
	
	/*public static void main(String[] args){
		LoginPanel lp = new LoginPanel();
	}*/
}