package Client;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import App.LoginPanel;
import custompanels.MessagePanel;
//<<<<<<< HEAD
//import utilities.Util;
//=======
import custompanels.NewsPanel;
//>>>>>>> bea5cf65fe242d709b066cfb414f23a7df1127b3
import Client.MainWindow;
import csci201.Constants;

public class ClientListener extends Thread{
	private Socket socket;
	private BufferedReader br;
	private PrintWriter pw;
	private MainWindow mainWindow;
	private LoginPanel loginPanel;
	private String userName;
	private boolean isAdded = false;
	
	public ClientListener(LoginPanel loginPanel, Socket socket){
		this.socket = socket;
		mainWindow = new MainWindow();
		this.mainWindow.setClientListener(this);
		this.loginPanel = loginPanel;
		boolean socketReady = initializeVariables();
		if (socketReady) {
			start();
		}
	}
	
	private boolean initializeVariables(){
		try {
			pw = new PrintWriter(socket.getOutputStream());
			br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException ioe) {
//			Util.printExceptionToCommand(ioe);
//			Util.printMessageToCommand("Could not get reader and writer for socket");
			return false;
		}
		
		return true;
	}
	
	public void sendMessage(String msg) {
		pw.println(msg);
		pw.flush();
	}
	
	public String getUserName(){
		return userName;
	}
	
	public void setUserName(String userName){
		this.userName = userName;
	}
	
	public void openMainWindow(){
		loginPanel.setVisible(false);
		mainWindow.setVisible(true);
	}
	
	public void exitApp(){
		System.out.println("closed");
		String message = "exit " + userName;
		sendMessage(message);
	}
	
	public void sendNewsString(String foodName){
		String newsString = Constants.getNewsString(userName, foodName);
		sendMessage(newsString);
	}
	
	public void run(){
		try {
			sendMessage("username "+userName);
			String line = br.readLine();
			while (line != null) { 
				String [] message = line.split(" ");
				String messageType = message[0];
				switch(messageType){
					case "chatmessage":
						MessagePanel msgp = mainWindow.getMessagePanel();
						msgp.addUser(message[1]);
						if(message.length>2){
							String msg = "";
							for(int i = 2; i < message.length; i++){
								msg += message[i] + " ";
							}
							msgp.setText(message[1], msg);
						}
						else
							msgp.setText(message[1], " ");
						break;
					case "NEWS":
						NewsPanel np = mainWindow.getNewsPanel();
						np.addANewsPost(line);
					case "exit":
						mainWindow.getMessagePanel().removeUser(message[1]);
						mainWindow.getMessagePanel().setExitText(message[1]);
						break;
				}	
				
				line = br.readLine();
			}
		} catch (IOException ioe) {
			System.out.println("Communication with the server failed.");
		}
	}

	public MainWindow getMainWindow() {
		return mainWindow;
	}
}
