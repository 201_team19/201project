package Client;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.*;

import Server.PortPanel;

public class HostAndPortPanel extends JFrame{
	private static final long serialVersionUID = 1L;
	private JTextField portTF, hostnameTF;
	private JLabel descriptionLabel, portLabel, hostnameLabel, errorLabel;
	private JButton connectButton;
	private Lock hostAndPortLock;
	private Condition hostAndPortCondition;
	private JPanel hostJP, portJP;
	private Socket socket;

	public HostAndPortPanel() {
		super("Host and Port");
		setSize(330,200);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		initializeVariables();
		createGUI();
		addActionAdapters();

		setVisible(true);
	}
	
	private void initializeVariables() {
		socket = null;
		descriptionLabel = new JLabel("<html>Enter the hostname and port number of the server</html>");
		portLabel = new JLabel("Port");
		hostnameLabel = new JLabel("Hostname");
		errorLabel = new JLabel();
		portJP = new JPanel();
		portTF = new JTextField(20);
		portTF.setText("6789");
		hostJP = new JPanel();
		hostnameTF = new JTextField(20);
		hostnameTF.setText("localhost");
		connectButton = new JButton("Connect");
		hostAndPortLock = new ReentrantLock();
		hostAndPortCondition = hostAndPortLock.newCondition();
		
	}
	
	private void createGUI() {
		setLayout(new GridLayout(5, 1));
		
		descriptionLabel.setHorizontalAlignment(JLabel.CENTER);
		add(descriptionLabel);
		add(errorLabel);
		
		hostJP.add(hostnameLabel);
		hostJP.add(hostnameTF);
		add(hostJP);
		
		portJP.add(portLabel);
		portJP.add(portTF);
		add(portJP);
		
		add(connectButton);
	}
	
	private void addActionAdapters() {
		class ConnectListener implements ActionListener {
			public void actionPerformed(ActionEvent ae) {
				String portStr = portTF.getText();
				int portInt = -1;
				try {
					portInt = Integer.parseInt(portStr);
				} catch (Exception e) {
					errorLabel.setText("Constants.portErrorString");
					return;
				}
				if (portInt > 0 && portInt < 65535) {
					// try to connect
					String hostnameStr = hostnameTF.getText();
					try {
						socket = new Socket(hostnameStr, portInt);
						hostAndPortLock.lock();
						hostAndPortCondition.signal();
						hostAndPortLock.unlock();
						HostAndPortPanel.this.setVisible(false);
					} catch (IOException ioe) {
						errorLabel.setText("Constants.unableToConnectString");
						ioe.printStackTrace();
						return;
					}
				}
				else { // port value out of range
					errorLabel.setText("Constants.portErrorString");
					return;
				}
			}
		}
		connectButton.addActionListener(new ConnectListener());
		hostnameTF.addActionListener(new ConnectListener());
		portTF.addActionListener(new ConnectListener());
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
	}
	
	public Socket getSocket() {
		while (socket == null) {
			hostAndPortLock.lock();
			try {
				hostAndPortCondition.await();
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
			hostAndPortLock.unlock();
		}
		return socket;
	}

}
