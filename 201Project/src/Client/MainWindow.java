package Client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

import Client.ClientListener;
import custompanels.FriendsPanel;
import custompanels.HomePanel;
import custompanels.MealsPanel;
import custompanels.MessagePanel;
import custompanels.NewsPanel;
import custompanels.ProgressPanel;

public class MainWindow extends JFrame{
	private static final long serialVersionUID = 1L;

	private JTabbedPane tabbedPane;
	private HomePanel hp;
	private NewsPanel np;
	private ProgressPanel pp;
	private MealsPanel mp;
	private FriendsPanel fp;
	private MessagePanel msgp;
	private ClientListener clientListener;

	
	public MainWindow(){
		super("Calorie App");
		setSize(600,400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		initializeVariables();
		createGUI();
		addActionAdapters();
	}
	
	private void initializeVariables(){
		tabbedPane = new JTabbedPane();
		hp = new HomePanel();
		np = new NewsPanel();
		pp = new ProgressPanel();
		mp = new MealsPanel(clientListener);
		fp = new FriendsPanel();
		msgp = new MessagePanel(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				String message = "chatmessage " + clientListener.getUserName() + " ";
				message += msgp.getMessage();
				clientListener.sendMessage(message);
				msgp.clearInput();
			}
			
		});
	}
	
	private void createGUI(){
		tabbedPane.add("Home", hp);
		tabbedPane.add("News", np);
		tabbedPane.add("Progress", pp);
		tabbedPane.add("Meals", mp);
		tabbedPane.add("Friends", fp);
		tabbedPane.add("Messages", msgp);
		add(tabbedPane);
	}
	
	private void addActionAdapters(){
		/*addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.out.println("closed");
				String message = "exit " + clientListener.getUserName();
				clientListener.sendMessage(message);
				System.exit(0);
			}
		});*/
	}
	
	public MessagePanel getMessagePanel(){
		return msgp;
	}
	
	public NewsPanel getNewsPanel(){
		return np;
	}
	
	public void setClientListener(ClientListener clientListener){
		this.clientListener = clientListener;
	}

	public MealsPanel getMealsPanel() {
		return mp;
	}
}
