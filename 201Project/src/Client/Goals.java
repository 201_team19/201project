package Client;

public class Goals {
	
	private boolean maintainWeight;
	private boolean loseWeight;
	private boolean gainWeight;
	
	private int _calories;
	private double _protein;
	private double _carbs;
	private double _fat;
	private int _goalWeight;
	private int _weeklyGoal;
	
	public Goals(){
		maintainWeight = false;
		loseWeight = false;
		gainWeight = false;
	}
	
	public void setWeightGoal(String Goal){ //change according to input
		if(Goal.equals("Maintain Weight"))
			maintainWeight = true;
		if(Goal.equals("Lose Weight"))
			loseWeight = true;
		if(Goal.equals("Gain Weight"))
			gainWeight = true;
	}
	
	public void setCalories(int calories){
		_calories = calories;
	}
	
	public int getCalories(){
		return _calories;
	}
	
	public void setGoalWeight(int goalWeight){
		_goalWeight = goalWeight;
	}
	
	public int getGoalWeight(){
		return _goalWeight;
	}
	
	public void setWeeklyGoal(int weeklyGoal){
		_weeklyGoal = weeklyGoal;
	}
	
	public int getWeeklyGoal(){
		return _weeklyGoal;
	}
	
	public void setProtein(double protein){
		_protein = protein;
	}
	
	public double getProtein(){
		return _protein;
	}
	
	public void setCarbs(double carbs){
		_carbs = carbs;
	}
	
	public double getCarbs(){
		return _carbs;
	}
	
	public void setFat(double fat){
		_fat = fat;
	}
	
	public double getFat(){
		return _fat;
	}

}
