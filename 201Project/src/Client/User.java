package Client;

import java.awt.Image;
import java.util.Vector;

public class User {
	
	private String _firstName;
	private String _lastName;
	private int _age;
	private String _sex;
	private int _weight;
	private int _height;
	private int _BMR;
	private Vector<User> friends;
	private Image _profImg;
	private Goals goals;
	
	public User(){
		goals = new Goals();
	}	
	
	public String getName(){
		return _firstName+" "+_lastName;
	}
	
	public void setName(String firstName, String lastName){
		_firstName = firstName;
		_lastName = lastName;
	}
	
	public int getAge(){
		return _age;
	}	
	
	public void setAge(int age){
		_age = age;
	}
	
	public String getSex(){
		return _sex;
	}
	
	public void setSex(String sex){
		_sex = sex;
	}
	
	public int getWeight(){
		return _weight;
	}
	
	public void setWeight(int weight){
		_weight = weight;
	}
	
	public int getHeight(){
		return _height;
	}
	
	public void setHeight(int height){
		_height = height;
	}
	
	public int getBMR(){
		return _BMR;
	}
	
	public void setBMR(int BMR){
		_BMR = BMR;
	}
	
	public void setGoal(String goal){ //change according to input
		goals.setWeightGoal(goal);
	}
	
	public int getCalories(){
		return goals.getCalories();
	}
	
	public void addFriend(User user){
		friends.add(user);
	}
	
	public void removeFriend(User user){
		friends.remove(user);
	}
	
	public Vector<User> getFriends(User user){
		return friends;
	}
	
	public Image getProfImg(){
		return _profImg;
	}
	
	public void setProfImg(Image profImg){
		_profImg = profImg;
	}

}
