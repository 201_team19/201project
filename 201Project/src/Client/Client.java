package Client;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.Socket;

import App.LoginPanel;
import Client.MainWindow;

public class Client {
	private LoginPanel loginPanel;
	private ClientListener clientListener;
	
	public Client() {
		HostAndPortPanel happ = new HostAndPortPanel();
		Socket socket = happ.getSocket();
		loginPanel = new LoginPanel(socket);
		//clientListener = new ClientListener(loginPanel, socket);
		/*mainWindow.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				clientListener.exitApp();
			}
		});*/
	}
	
	public static void main(String [] args){
		new Client();
	}
}
