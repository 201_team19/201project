package csci201;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class JDBCDriver {

	
	Connection conn = null;
	
	public JDBCDriver(){
		
	}
	public void addToDatabase(FoodItem food){
		System.out.println("adding " +food.getName()+" to databse inside jdbc driver");
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/FoodItems?user=root&password=");
			PreparedStatement ps = conn.prepareStatement("INSERT INTO FoodItems (fname, calories, carbs, fats, sugar, protein, fiber) VALUES (?, ? ,? ,?, ?, ? ,?);");
			ps.setString(1, food.getName());
			ps.setString(2,"" +food.getCalories());
			ps.setString(3, "" +food.getCarbs());
			ps.setString(4, "" +food.getFats());
			ps.setString(5, "" +food.getSugars());
			ps.setString(6, "" +food.getProtein());
			ps.setString(7, "" +food.getFiber());
			ps.executeUpdate();
		}catch(ClassNotFoundException cnfe){
			System.out.println("cnfe: " + cnfe.getMessage());
		}catch(SQLException sqle){
			System.out.println("sqle: " + sqle.getMessage());
		}
		finally{
			if(conn != null){
				try{
					conn.close();
				} catch(SQLException sqle){
					System.out.println ("sqle closing conn: " + sqle.getMessage());

				}
			}
		}
	}
	public void addUserToDatabase(User user){
		System.out.println("adding " +user.getName()+" to databse inside jdbc driver");
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/UserInfo?user=root&password=");
			PreparedStatement ps = conn.prepareStatement("INSERT INTO UserInfo (Username, Password, Name, Age, Weight, Height, Gender) VALUES (?, ? ,? ,?, ?, ? ,?);");
			ps.setString(1, user.getUsername());
			ps.setString(2,"" +user.getPassword());
			ps.setString(3, "" +user.getName());
			ps.setString(4, "" +user.getAge());
			ps.setString(5, "" +user.getWeight());
			ps.setString(6, "" +user.getHeight());
			ps.setString(7, "" +user.getGender());
			ps.executeUpdate();
		}catch(ClassNotFoundException cnfe){
			System.out.println("cnfe: " + cnfe.getMessage());
			cnfe.printStackTrace();
		}catch(SQLException sqle){
			System.out.println("sqle: " + sqle.getMessage());
		}
		finally{
			if(conn != null){
				try{
					conn.close();
				} catch(SQLException sqle){
					System.out.println ("sqle closing conn: " + sqle.getMessage());

				}
			}
		}
	}
	
	public boolean containsLogIn(String username, String password){
		boolean returner = false;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/FoodItems?user=root&password=");
			PreparedStatement ps = conn.prepareStatement("SELECT  f.fname"
					+ "FROM Login f "
					+ "WHERE f.username=? AND f.password=?;");
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			int counter = 0; 
			while(rs.next()){
				counter ++;
			}
			if(counter != 0){
				returner = true;
			}
			rs.close();
			
		}catch(ClassNotFoundException cnfe){
			System.out.println("cnfe: " + cnfe.getMessage());
		}catch(SQLException sqle){
			System.out.println("sqle: " + sqle.getMessage());
		}
		finally{
			if(conn != null){
				try{
					conn.close();
				} catch(SQLException sqle){
					System.out.println ("sqle closing conn: " + sqle.getMessage());

				}
			}
		}
		return returner;
	}
	
	public boolean containsUsername(String username){
		boolean returner = false;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/FoodItems?user=root&password=");
			PreparedStatement ps = conn.prepareStatement("SELECT  f.fname"
					+ "FROM Login f "
					+ "WHERE f.username=?;");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			int counter = 0; 
			while(rs.next()){
				counter ++;
			}
			if(counter != 0){
				returner = true;
			}
			rs.close();
			
		}catch(ClassNotFoundException cnfe){
			System.out.println("cnfe: " + cnfe.getMessage());
		}catch(SQLException sqle){
			System.out.println("sqle: " + sqle.getMessage());
		}
		finally{
			if(conn != null){
				try{
					conn.close();
				} catch(SQLException sqle){
					System.out.println ("sqle closing conn: " + sqle.getMessage());

				}
			}
		}
		return returner;
	}
	
	public Vector<FoodItem> searchFor(String foodName){ 
		Vector<FoodItem> foodReturner = new Vector<FoodItem>();
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/FoodItems?user=root&password=");
			PreparedStatement ps = conn.prepareStatement("SELECT  f.fname"
					+ "FROM FoodItem f "
					+ "WHERE f.fname=? ;");
			ps.setString(1, foodName);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				String fname = rs.getString("fname");
				double calories = rs.getDouble("calories");
				double carbs = rs.getDouble("carbs");
				double fats = rs.getDouble("fat");
				double sugar = rs.getDouble("sugar");
				double protein = rs.getDouble("protein");
				double fiber = rs.getDouble("fiber");
				FoodItem tempFood = new FoodItem(fname, calories, carbs, fats, sugar, protein,fiber);
				foodReturner.add(tempFood);
			}
			rs.close();
			
		}catch(ClassNotFoundException cnfe){
			System.out.println("cnfe: " + cnfe.getMessage());
		}catch(SQLException sqle){
			System.out.println("sqle: " + sqle.getMessage());
		}
		finally{
			if(conn != null){
				try{
					conn.close();
				} catch(SQLException sqle){
					System.out.println ("sqle closing conn: " + sqle.getMessage());

				}
			}
		}
		return foodReturner;
		
	}
}