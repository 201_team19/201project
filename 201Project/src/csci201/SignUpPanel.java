//package Client;
package csci201;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class SignUpPanel extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private JPanel jp, firstJP, secondJP, thirdJP, fourthJP, fifthJP, sixthJP,seventhJP, eighthJP, errorJP;
	private JLabel usernameLabel, passwordLabel, FnameLabel,LnameLabel, ageLabel, weightLabel, heightLabel, genderLabel, errorLabel;
	private JTextField usernameTF, passwordTF, FnameTF, LnameTF, ageTF, weightTF;
	private JComboBox heightCombo, genderCombo;
	private JButton continueButton;
	private JDBCDriver jdbcDriver = new JDBCDriver(); 
	String[] height = { "5 ' 0 \" ", "5 ' 1 \"","5 ' 2 \" ", "5 ' 3 \" ", "5 ' 4 \" ", "5 ' 5 \" ", 
			"5 ' 6 \" ", "5 ' 7 \" ", "5 ' 8 \" ", "5 ' 9 \" ", "5 ' 10 \" ", "5 ' 11 \" ", "6 ' 0 \" ", 
			"6 ' 1 \" ", "6 ' 2 \" ", "6 ' 3 \" ", "6 ' 4 \" ", "6 ' 5 \" ", "6 ' 6 \" ", "6 ' 7 \" ", 
			"6 ' 8 \" ", "6 ' 9 \" ", "6 ' 10 \" ", "6 ' 11 \" ", "7 ' 0 \" ", "7 ' 1 \" ", "7 ' 2 \" ", };
	String[] gender = { "female", "male" };
	
	public SignUpPanel(){
		super("Sign Up");
		setSize(500,500);
		setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		
		createGUI();
		addActionAdapters();
		
		setVisible(true);
	}
	
	private void createGUI(){
		jp = new JPanel(new GridLayout(9,1));
		jp.setBorder(BorderFactory.createEmptyBorder(15,5,5,5));
		add(jp);
		
		firstJP = new JPanel();
		usernameLabel = new JLabel("Username:");
		usernameTF = new JTextField(15);
		firstJP.add(usernameLabel);
		firstJP.add(usernameTF);
		jp.add(firstJP);
		
		secondJP = new JPanel();
		passwordLabel = new JLabel("Password:");
		passwordTF = new JTextField(15);
		secondJP.add(passwordLabel);
		secondJP.add(passwordTF);
		jp.add(secondJP);
		
		thirdJP = new JPanel();
		FnameTF = new JTextField(7);
		LnameTF= new JTextField(7);
		FnameLabel = new JLabel("First Name:");
		LnameLabel = new JLabel("Last Name:");
		thirdJP.add(FnameLabel);
		thirdJP.add(FnameTF);
		thirdJP.add(LnameLabel);
		thirdJP.add(LnameTF);
		jp.add(thirdJP);	
		
		fourthJP = new JPanel();
		ageLabel = new JLabel("Age:");
		ageTF = new JTextField(15);
		fourthJP.add(ageLabel);
		fourthJP.add(ageTF);
		jp.add(fourthJP);
		
		fifthJP = new JPanel();
		weightTF = new JTextField(15);
		weightLabel = new JLabel("Weight:");
		fifthJP.add(weightLabel);
		fifthJP.add(weightTF);
		jp.add(fifthJP);
		
		sixthJP = new JPanel();
		
		heightCombo = new JComboBox(height);
		heightLabel = new JLabel("height");
		sixthJP.add(heightLabel);
		sixthJP.add(heightCombo);
		jp.add(sixthJP);
		
		seventhJP = new JPanel();
		genderLabel = new JLabel("Gender:");
		
		genderCombo = new JComboBox(gender);
		seventhJP.add(genderLabel);
		seventhJP.add(genderCombo);
		jp.add(seventhJP);
		
		eighthJP = new JPanel();
		continueButton = new JButton("Continue");
		eighthJP.add(continueButton);
		jp.add(eighthJP);
		
		errorJP = new JPanel();
		errorLabel = new JLabel("");
		errorJP.add(errorLabel);
		jp.add(errorJP);
				

		
	}
	
	private void addActionAdapters(){
		continueButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean goodToGo = true;
				String errorText ="";
				if(jdbcDriver.containsUsername(usernameTF.getText())){
					errorLabel.setText("This username is already in the database");
				}
				else{
					if (!ageTF.getText().matches("\\d+")){
						errorText += "Please enter ONLY DIGITS in the Age text field. ";
						goodToGo = false;
					}
					if (!weightTF.getText().matches("\\d+")){
						errorText += "Please enter ONLY DIGITS in the Weight text field";
						goodToGo = false;
					}
					errorLabel.setText(errorText);
					errorLabel.setForeground(Color.RED);
					revalidate();
					repaint();
				}
				if(goodToGo == true){
					
					int ageInteger = Integer.parseInt(ageTF.getText());
					int weightInteger = Integer.parseInt(weightTF.getText());
					int heightInInches = Constants.getHeightInInches(height[heightCombo.getSelectedIndex()]);
					jdbcDriver.addUserToDatabase(new User(usernameTF.getText(), passwordTF.getText(), FnameTF.getText() + " " + LnameTF.getText(), ageInteger, weightInteger, heightInInches, gender[genderCombo.getSelectedIndex()]));
					usernameTF.setText("");
					passwordTF.setText("");
					FnameTF.setText("");
					LnameTF.setText("");
					ageTF.setText("");
					weightTF.setText("");
				}
				
			}
		});	
	
	}
	
	public static void main(String[] args){
		SignUpPanel lp = new SignUpPanel();
	}
}