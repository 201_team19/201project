package csci201;

public class Constants {

	//NEWS "username" "foodname" 
	public static String getUsernameFromNewsString(String news){
		String[] newsARR = news.split("\\s+");
		return  newsARR[1];
	}
	
	public static String getMealTimeFromNewsString(String news){
		String[] newsARR = news.split("\\s+");
		return newsARR[newsARR.length -1];
	}
	
	public static String getFoodNameFromNewsString(String news){
		String[] newsARR = news.split("\\s+");
		String temp = "";
		for(int i = 2; i < newsARR.length; i++){
			temp += newsARR[i]+ " ";
		}
		return temp; 
	}
	
	public static String getNewsString(String userName, String foodName){
		String temp = "NEWS  " + userName + " " + foodName;
		return temp;
	}
	
	public static String getTwelveHourTime(String time){
		String[] timeARR = time.split(":");
		String newTime = "";
			int timeInt = Integer.parseInt(timeARR[0]);
			if(timeInt >= 13){
				timeInt = timeInt - 12;
			}
			timeARR[0] = "" + timeInt;
		
		for(int i = 0; i < timeARR.length; i++){
			newTime += timeARR[i] ;
			if(i != timeARR.length-1){
				newTime += ":";
			}
		}
		return newTime;

	}
	

	
	public static int getHeightInInches(String height){
		String[] heightARR = height.split("\\s+");
		int feet = Integer.parseInt(heightARR[0]);
		int inches = Integer.parseInt(heightARR[2]);
		int totalHeight = (feet*12) + inches;
		return totalHeight;
	}
	
	
}
