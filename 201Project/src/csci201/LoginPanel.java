//package Client;
package csci201;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class LoginPanel extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private JPanel jp, firstJP, secondJP, thirdJP, errorJP;
	private JLabel usernameLabel, passwordLabel;
	private JTextField usernameTF, passwordTF;
	private JButton signInButton, signUpButton;
	private JDBCDriver jdbcDriver = new JDBCDriver(); 
	
	public LoginPanel(){
		super("Login");
		setSize(300,150);
		setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		
		createGUI();
		addActionAdapters();
		
		setVisible(true);
	}
	
	private void createGUI(){
		jp = new JPanel(new GridLayout(3,1));
		jp.setBorder(BorderFactory.createEmptyBorder(15,5,5,5));
		add(jp);
		
		firstJP = new JPanel();
		usernameLabel = new JLabel("Username:");
		usernameTF = new JTextField(15);
		firstJP.add(usernameLabel);
		firstJP.add(usernameTF);
		jp.add(firstJP);
		
		secondJP = new JPanel();
		passwordLabel = new JLabel("Password:");
		passwordTF = new JTextField(15);
		secondJP.add(passwordLabel);
		secondJP.add(passwordTF);
		jp.add(secondJP);
		
		thirdJP = new JPanel();
		signInButton = new JButton("Sign in");
		signUpButton = new JButton("Sign up");
		thirdJP.add(signInButton);
		thirdJP.add(signUpButton);
		jp.add(thirdJP);	
	}
	
	private void addActionAdapters(){
		signInButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(jdbcDriver.containsLogIn(usernameTF.getText(), passwordTF.getText())){
					// GO TO NEXT PAGE!!! s
				}
				
			}
		});	
		signUpButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(jdbcDriver.containsLogIn(usernameTF.getText(), passwordTF.getText())){
					// GO TO NEXT PAGE!!! s
				}
				
			}
		});	
	}
	
	public static void main(String[] args){
		LoginPanel lp = new LoginPanel();
	}
}